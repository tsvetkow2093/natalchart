<?php

class Calculation_Astrology_Abstract {

    public $signOffset = array(
        'pisces' => 0,
        'aquarius' => 30,
        'capricorn' => 60,
        'sagittarius' => 90,
        'scorpio' => 120,
        'libra' => 150,
        'virgo' => 180,
        'leo' => 210,
        'cancer' => 240,
        'gemini' => 270,
        'taurus' => 300,
        'aries' => 330,
    );

    /**
     * Set the Sign offset Array
     */
    public $signFont = array(
        'pisces' => 'c',// '&#99;',
        'aquarius' => 'x',// '&#120;',
        'capricorn' => 'v',// '&#118;',
        'sagittarius' => 'l',// '&#108;',
        'scorpio' => 'k',// '&#107;',
        'libra' => 'j',// '&#106;',
        'virgo' => 'h',// '&#104;',
        'leo' => 'g',// '&#103;',
        'cancer' => 'f',// '&#102;',
        'gemini' => 'd',// '&#100;',
        'taurus' => 's',// '&#115;',
        'aries' => 'a',// '&#97;',
        'sun' => 'Q',// '&#81;',
        'moon' => 'W',// '&#87;',
        'ascendant' => 'Z',// '&#90;',
        'mc (midheaven)' => 'X',// '&#88;',
        'chiron' => 'M',// '&#77;',
        'true node' => '{',// '&#141;',
        'p. of fortune' => '<', //'&#60;',
        'retrograde' => '>', //'&#62;',
        'mercury' => 'E',// '&#69;',
        'uranus' => 'I',// '&#73;',
        'neptune' => 'O',// '&#79;',
        'pluto' => 'P',// '&#80;',
        'venus' => 'R',// '&#82;',
        'mars' => 'T',// '&#84;',
        'saturn' => 'U',// '&#85;',
        'jupiter' => 'Y',// '&#89;',
        'pluto' => '„',// '&#8222;',
        'lilith' => '²',// '&#178;',
        'vertex' => '•',// '&#149;',
        'descendant' => '’',// '&#8217;',
        'ic' => '“',// '&#8220;',
        'trine' => 'e',// '&#101;',
        'sextile' => 't',// '&#116;',
        'square' => 'r',// '&#114;',
        'quincunx' => 'o',// '&#111;',
        'conjunction' => 'q',// '&#113;',
        'opposition' => 'w',// '&#119;',
        'semisquare' => 'y',// '&#121;',
        'semisextile' => 'i',// '&#105;',
        'sesquiquare' => 'u',// '&#117;',
        /* This is the European versions because I can't figure out how to display the N. American Quintile character */
        'quintile' => '[',// '&#91;',
        'biquintile' => ']',// '&#93;', /* &#353; for North American */

    );
}
?>