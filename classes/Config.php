<?php
	class Config {
		//const APPLICATION_ENV = "production";
		const APPLICATION_ENV = "development";
		const SWEPH_PATH = "/opt/swe/";
		const SWEPH_SAMPLE_PATH = "library/Sweph/sample-out.txt";
		const FONTS_PATH = "fonts/";
		const TEMP_PATH = "temp/";
		const HOST_NAME = "192.168.10.10";
		const DB_USER = "homestead";
		const DB_PASSWORD = "secret";
		const DB_NAME = "natalcart";
	}
?>