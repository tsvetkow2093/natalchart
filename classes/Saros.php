<?php

class Saros
{
    protected $sarosLunar = 'saros_lunar';
	protected $sarosSolar = 'saros_solar';
	protected $eclipsesLunar = 'eclipses_lunar';
	protected $eclipsesSolar = 'eclipses_solar';
            
    public function __construct() {
		
    }
	
	public function getSarosLunar( $saros ) {		
		$query = "SELECT * FROM `$this->sarosLunar` WHERE `saros`='" . $saros . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}

	public function getSarosSolar( $saros ) {		
		$query = "SELECT * FROM `$this->sarosSolar` WHERE `saros`='" . $saros . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}	
     
	public function getEclipsesLunar( $date ) {		
		$query = "SELECT * FROM `$this->eclipsesLunar` WHERE `date`='" . $date . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}

	public function getEclipsesSolar( $date ) {		
		$query = "SELECT * FROM `$this->eclipsesSolar` WHERE `date`='" . $date . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}	
}
?>
