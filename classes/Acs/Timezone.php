<?php

class Acs_Timezone {
	
	private $_base;
	private $_checking;
	private $_type;
	
	public function __construct($space, $time) {
		
		if (($space) && ($time instanceof DateTime))
			$this->loadFromSpaceTime($space, $time);
		
	}
	
	public function getBase() {
		return $this->_base;
	}
	
	public function getChecking() {
		return $this->_checking;
	}
	
	public function getType() {
		return $this->_type;
	}
	
	public function loadFromSpaceTime($space, $time) {
    	
    	$hour = $time->format('H');
    	$minute = $time->format('m');
    	$day = $time->format('d');
    	$month = $time->format('M');
    	$year = $time->format('Y');	
    		
    	$searchDate = ($year * 624000) + ($month * 48000) + ($day * 1500) + ($hour * 60) + $minute;
    	
        // Step 1: Set the zone to city Field 6 (time_zone)
        $theZone = $space->time_zone;

        $timezoneTable = new Acs_Timezone_Table();
        
        // Step 2: If the zone is < 12000, go to step 7.
        while ($theZone >= 12000) {

			//echo "-1-<br>";
		
            // Step 3: Subtract 12000 from the zone and look up that table number.

            $tableNumberLookup = $theZone - 12000;
			
			$queryResult = $timezoneTable->getTableNumbers( $tableNumberLookup );

			// echo "<pre>";
			// var_dump( $queryResult );
			// echo "</pre>";			
			
            // Step 4: Look through the dates in the table until you find a pair between which
            // the date for which you are searching falls.  If your date is before the
            // first date in the table, the zone is undefined (i.e., the city was not
            // yet using Standard Time); set the zone to the longitude multiplied by 60
            // (to make it equivalent to hours from Greenwich * 900) and go to step 7.

            $changeDate    = null;
            $timeType      = null;

			while( $tableNumberRow = mysqli_fetch_object( $queryResult ) ) {
				
				//echo "-2-<br>";

				// echo "<pre>";
				// var_dump( $tableNumberRow );
				// echo "</pre>";			
			
                if ($tableNumberRow->change_date > $searchDate) {
					// echo "<pre>";
					// var_dump( $tableNumberRow->change_date );
					// echo "</pre>";
				
					break;
				}

                $changeDate    = $tableNumberRow->change_date;
                $timeType      = $tableNumberRow->time_type;

            }

            if ($changeDate === null) {

                // Step 4 (Alternate Route)
                // set the zone to the longitude multiplied by 60
                // (to make it equivalent to hours from Greenwich * 900) and go to step 7.

                $longitudeArray = array();
                preg_match('/([0-9]*)(E|W)([0-9]*)\'([0-9]*)/', $space->longitude, $longitudeArray);

                $longitudeToDecimal = floatval($longitudeArray[1]) + ((floatval($longitudeArray[3]) + (floatval($longitudeArray[4])/60)) / 60);
                $theZone = ($longitudeToDecimal * 60); // Or ($longitudeToDecimal * 900) for "Hours from Greenwich"

                break; // Effectively getting out of the Loop and Going to Step 7.

            } else {

                // Step 5: Set the zone to the value for the earlier date bracketing your date.

                $theZone = $timeType;

            }

        }   // Step 6: Go to Step 2

        $this->_base = $theZone;

        // Step 7: Set the type to city Field 7.
        $theType = $space->time_type;

        // Step 8: If the type is < 50, goto step 14.
        while ($theType > 50) {
			
			//echo "-3-<br>";
			
			// echo "<pre>";
			// var_dump( $theType );
			// echo "</pre>";			

            // Step 9: If the type is > 30000, goto step 18.
            if ($theType > 30000) {

                // Step 18: If the type is 30002, then if the zone is 4500 (Eastern) set the type to 0
                // (Standard); otherwise set the type to 1 (Daylight).  You are done.
                if ($theType == 30002) {

                    $this->_checking = 30002;

                    if ($theZone == 4500) {
                        $theType = 0;
                    } else {
                        $theType = 1;
                    }

                    break;

                }

                // Step 19: If the type is 30003, then if the zone is 4500 (Eastern) set the type to 0
                // (Standard) and you are done; otherwise set the type to 30001.
                if ($theType == 30003) {

                    $this->_checking = 30003;

                    if ($theZone == 4500) {
                        $theType = 0;
                        break;
                    } else {
                        $theType = 30001;
                    }

                }

                // Step 20: If the type is 30001, the city is on the US standard table: before 1987
                // if the date is before the last Sunday in April or after the last Sunday
                // in October of the year, set the type to 0 (Standard); otherwise set the
                // type to 1 (Daylight).  For 1987 through 2005, the switch dates are instead
                // the first Sunday in April and the last Sunday in October.  Starting in 2006,
                // use the second Sunday in March and the first Sunday in November for change
                // dates.
                if ($theType == 30001) {

                    $this->_checking = (empty($this->_checking) ? 30001 : $this->_checking);

                    if ($year < 1987) {

                        if (
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) < date('md', strtotime('last sunday', strtotime('May 1st ' . $year))))
                            ||
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) > date('md', strtotime('last sunday', strtotime('November 1st ' . $year))) )
                           )
                        {
                            $theType = 0;
                        } else {
                            $theType = 1;
                        }

                    } else if ($year >= 1987 && $year <= 2005) {

                        if (
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) < date('md', strtotime('first sunday', strtotime('April ' . $year))))
                            ||
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) > date('md', strtotime('last sunday', strtotime('November 1st ' . $year))) )
                           )
                        {
                            $theType = 0;
                        } else {
                            $theType = 1;
                        }

                    } else { // if year >= 2006

                        if (
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) < date('md', strtotime('second sunday', strtotime('April ' . $year))))
                            ||
                            (date('md', mktime($hour, $minute, 0, $month, $day, $year)) > date('md', strtotime('first sunday', strtotime('November ' . $year))) )
                           )
                        {
                            $theType = 0;
                        } else {
                            $theType = 1;
                        }

                    }

                    break;

                }

            // At this point theType is less than 30,000 and greater than 50.
            } else {

                // Step 10: Subtract 50 from the type and look up that table number.

                $tableNumberLookup = $theType - 50;
				
				// echo "<pre>";				
				// var_dump($tableNumberLookup);
				// echo "</pre>";				
				
				$queryResult = $timezoneTable->getTableNumbers( $tableNumberLookup );

				// echo "<pre>";				
				// var_dump($queryResult);
				// echo "</pre>";	
                
                // Step 11: Look through the dates in the table until you find a pair between which
                // the date for which you are searching falls.

                $tableNumberIteration   = 1;

				while( $tableNumberRow = mysqli_fetch_object( $queryResult ) ) {
					
					//echo "-4-<br>";
					
					// echo "<pre>";				
					// var_dump($searchDate);
					// echo "</pre>";						
					
                    // If your date is before the first date in the table, set the type to 4
                    // (Local Mean Time), and you are done.
                    if ( ($searchDate < $tableNumberRow->change_date) && ($tableNumberIteration == 1)) {
                    	$this->_type = 4;
						
						if ( ($tableNumberRow->time_type == 2524) or ($tableNumberRow->time_type == 2528) or
							 ($tableNumberRow->time_type == 2529) or ($tableNumberRow->time_type == 3092) or ($tableNumberRow->time_type == 3093)							 
						) {
							$theType = 4;
						}
						
						// echo "<pre>searchDate ";				
						// var_dump($searchDate);
						// echo "</pre>";

						// echo "<pre>tableNumberRow->change_date ";				
						// var_dump($tableNumberRow->change_date);
						// echo "</pre>";						
						
                    	break;
                    }

                    // If the value in the first table entry is 30004, it is Illinois.  Illinois
                    // had a law that births should be recorded in Standard time, even when
                    // Daylight time was in effect.  The law was on the books from 1916 through
                    // June, 1959, except during World War II.  If you are using the tables to find
                    // the time type in order to get the actual time that a person was born, you
                    // probably want to assume the law was observed (absent any knowledge to the
                    // contrary).  In that case, set the type to 91 (Table 41) and go to step 8.
                    // If you are using the tables to find what time type was actually in effect,
                    // then just fall through to the next step.  The Illinois tables all have the
                    // same date for the first (30004) and second entries, so a date will never be
                    // bracketed between them.
                    if ( ($tableNumberRow->time_type == 30004) && ($tableNumberIteration == 1)) {
                        $this->_checking = 30004;
                        $theType = 91;
                        continue;
                        // We can do something special here if we want...
                    }

                    // If the value in the first table entry is 30005, it is Pennsylvania.  That
                    // state had a law like the Illinois law, but researchers believe that it was
                    // not generally observed.  You can, if you wish, put up a warning message.
                    // The recommended procedure is to just fall through to step 11.
                    if ( ($tableNumberRow->time_type == 30005) && ($tableNumberIteration == 1)) {
                        $this->_checking = 30005;
                        // We can do something special here if we want...
                    }

                    if ($tableNumberRow->change_date > $searchDate) break;

                    // Step 12: Set the type to the value for the earlier date bracketing your date.
                    $theType = $tableNumberRow->time_type;

                }

            }


        } // Step 13: Go to Step 8

        // Step 14: If the type is 4, subtract 450 (1/2 hour) from the zone and set the type
        // to 0 (Standard).  You are done.
        if ($theType == 4) {
        	$this->_base     -= 450;
        	$theType     = 0;
        }

        // Step 15: If the type is 6, subtract 300 (20 minutes) from the zone and set the type
        // to 0 (Standard).  You are done.
        if ($theType == 6) {
        	$this->_base     -= 300;
        	$theType     = 0;
        }

        // Step 16: If the type is 7, subtract 600 (40 minutes) from the zone and set the type
        // to 0 (Standard).  You are done.
        if ($theType == 7) {
        	$this->_base     -= 600;
        	$theType     = 0;
        }

        // Make sure we have a value for it.
        $this->_type = $theType;
    	
    	
 	}
	
}
?>