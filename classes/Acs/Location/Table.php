<?php

class Acs_Location_Table
{
    /** Table name */
    protected $_name    = 'acs_location';
    protected $_primary    = 'id';
            
    public function __construct() {
    	
    }		

	public function getLocationRow( $locationID ) {	
		$query = "SELECT * FROM `$this->_name` WHERE `$this->_primary`='" . $locationID . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );
		$row = mysqli_fetch_object( $queryResult );

		return $row;
	}
	
	public function locationSearch( $cityName ) {
		$db = new DB(  );
		$dbConnection = $db->getDBConnection(  );
		
		$cityName = $dbConnection->real_escape_string( $cityName );
		
		$query = "SELECT * FROM `$this->_name` WHERE `city` LIKE '" . $cityName . "%' LIMIT 60";
		
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}

	public function getCityInfo( $cityID ) {
		$db = new DB(  );
		$dbConnection = $db->getDBConnection(  );
		
		$cityID = $dbConnection->real_escape_string( $cityID );
		
		$query = "SELECT * FROM `$this->_name` WHERE `$this->_primary`='" . $cityID . "'";
		
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}	
}
?>