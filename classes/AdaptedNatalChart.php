<?php

class AdaptedNatalChart extends Calculation_Astrology_NatalChart
{
    public $kSortedPlanets;
    public $copyKSortedPlanets;
    public $planetCollisionLimit = 5;
    public $videoPlanetCollisionLimit = 5;
    public $ascendantDiff;

    // массив для вывода данных через js
    public $output = [];

    public function __construct($data = null)
    {
        if (!is_null($data))
            $this->setFromArray($data);

        $this->swephPath = Config::SWEPH_PATH;

        $this->_birthDate = $this->getBirthDate();
        $this->_location = $this->getLocationRow();
        $this->_timezone = $this->getTimezone();
        $this->calculate();
        $this->initializeChart();
    }

    public function calculate()
    {

        $month = $this->getBirthDate('m');
        $day = $this->getBirthDate('j');
        $year = $this->getBirthDate('Y');

        $hour = $this->getBirthDate('H');
        $minute = $this->getBirthDate('i');

        if (($hour == 12) and ($minute == 0)) {
            $minute = 1;
        }

        // var_dump($month);
        // var_dump($day );
        // var_dump($year);
        // var_dump($hour);
        // var_dump($minute);

        // Need to add -1 or +1 depending on DST.
        $timezone = $this->getTimezone()->getBase() / 900;
        $timezone = (($this->getTimezone()->getType() == 1) && ($this->getTimezone()->getBase() < 0) ? $timezone - 1 : $timezone);

        // Get Longitude Values
        $longitudeArray = array();
        preg_match('/([0-9]*)(E|W)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->longitude, $longitudeArray);

        $long_deg = intval($longitudeArray[1]);
        $long_min = intval($longitudeArray[3]);
        $ew = ($longitudeArray[2] == 'E' ? 1 : -1);

        // Get Latitude Values
        $latitudeArray = array();
        preg_match('/([0-9]*)(N|S)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->latitude, $latitudeArray);

        $lat_deg = intval($latitudeArray[1]);
        $lat_min = intval($latitudeArray[3]);
        $ns = ($latitudeArray[2] == 'N' ? 1 : -1);

        // TODO: Move: calculate astronomic data
        $swephsrc = 'sweph';
        $sweph = 'sweph';

        //assign data from database to local variables
        $inmonth = $month;
        $inday = $day;
        $inyear = $year;

        $inhours = $hour;
        $inmins = $minute;
        $insecs = "0";

        $intz = $timezone * -1;

        $my_longitude = $ew * ($long_deg + ($long_min / 60));
        $my_latitude = $ns * ($lat_deg + ($lat_min / 60));

        if ($intz >= 0) {
            $whole = floor($intz);
            $fraction = $intz - floor($intz);
        } else {
            $whole = ceil($intz);
            $fraction = $intz - ceil($intz);
        }

        $inhours = $inhours - $whole;
        $inmins = $inmins - ($fraction * 60);

        // var_dump($inhours);
        // var_dump($inmins );
        // var_dump($insecs);
        // var_dump($inmonth);
        // var_dump($inday);
        // var_dump($inyear);

        // adjust date and time for minus hour due to time zone taking the hour negative
        $utdatenow = strftime("%d.%m.%Y", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));
        $utnow = strftime("%H:%M:%S", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));

        // get LAST_PLANET planets and all house cusps
        $h_sys = "p";

        if (Config::APPLICATION_ENV != 'production') {
            $out = file(Config::SWEPH_SAMPLE_PATH);
        } else {
            exec($this->swephPath . "swetest -edir" . $this->swephPath . " -b$utdatenow -ut$utnow -p0123456789DAttt -eswe -house$my_longitude,$my_latitude,$h_sys -flsj -g, -head", $out);
        }

        // var_dump($utdatenow);
        // var_dump($utnow);
        // var_dump($my_longitude);
        // var_dump($my_latitude);
        // var_dump($h_sys);

        // Each line of output data from swetest is exploded into array $row, giving these elements:
        // 0 = longitude
        // 1 = speed
        // 2 = house position
        // planets are index 0 - index (LAST_PLANET), house cusps are index (LAST_PLANET + 1) - (LAST_PLANET + 12)
        $longitude1 = array();
        $speed1 = array();
        $house_pos1 = array();
        foreach ($out as $key => $line) {
            $row = explode(',', $line);

            $longitude1[$key] = floatval($row[0]);
            $speed1[$key] = floatval($row[1]);
            $house_pos1[$key] = (isset($row[2]) ? floatval($row[2]) : 0);
        }

        //include("constants.php");			// this is here because we must rename the planet names

        $pl_name = array();
        $pl_name[0] = "Sun";
        $pl_name[1] = "Moon";
        $pl_name[2] = "Mercury";
        $pl_name[3] = "Venus";
        $pl_name[4] = "Mars";
        $pl_name[5] = "Jupiter";
        $pl_name[6] = "Saturn";
        $pl_name[7] = "Uranus";
        $pl_name[8] = "Neptune";
        $pl_name[9] = "Pluto";
        $pl_name[10] = "Chiron";
        $pl_name[11] = "Lilith";
        $pl_name[12] = "True Node";
        $pl_name[13] = "P. of Fortune";        //add a planet
        $pl_name[14] = "Vertex";
        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "Midheaven";

        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "House 2";
        $pl_name[self::LAST_PLANET + 3] = "House 3";
        $pl_name[self::LAST_PLANET + 4] = "House 4";
        $pl_name[self::LAST_PLANET + 5] = "House 5";
        $pl_name[self::LAST_PLANET + 6] = "House 6";
        $pl_name[self::LAST_PLANET + 7] = "House 7";
        $pl_name[self::LAST_PLANET + 8] = "House 8";
        $pl_name[self::LAST_PLANET + 9] = "House 9";
        $pl_name[self::LAST_PLANET + 10] = "MC (Midheaven)";
        $pl_name[self::LAST_PLANET + 11] = "House 11";
        $pl_name[self::LAST_PLANET + 12] = "House 12";

        //calculate the Part of Fortune
        //is this a day chart or a night chart?
        if ($longitude1[self::LAST_PLANET + 1] > $longitude1[self::LAST_PLANET + 7]) {

            if ($longitude1[0] <= $longitude1[self::LAST_PLANET + 1] && $longitude1[0] > $longitude1[self::LAST_PLANET + 7]) {
                $day_chart = true;
            } else {
                $day_chart = false;
            }

        } else {

            if ($longitude1[0] > $longitude1[self::LAST_PLANET + 1] && $longitude1[0] <= $longitude1[self::LAST_PLANET + 7]) {
                $day_chart = false;
            } else {
                $day_chart = true;
            }

        }

        if ($day_chart == true) {
            $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] + $longitude1[1] - $longitude1[0];
        } else {
            $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] - $longitude1[1] + $longitude1[0];
        }

        if ($longitude1[self::SE_POF] >= 360) {
            $longitude1[self::SE_POF] = $longitude1[self::SE_POF] - 360;
        }

        if ($longitude1[self::SE_POF] < 0) {
            $longitude1[self::SE_POF] = $longitude1[self::SE_POF] + 360;
        }

        //add a planet - maybe some code needs to be put here

        //capture the Vertex longitude
        $longitude1[self::LAST_PLANET] = $longitude1[self::LAST_PLANET + 16];        //Asc = +13, MC = +14, RAMC = +15, Vertex = +16


        //get house positions of planets here
        for ($x = 1; $x <= 12; $x++) {

            for ($y = 0; $y <= self::LAST_PLANET; $y++) {

                $pl = $longitude1[$y] + (1 / 36000);

                if ($x < 12 && $longitude1[$x + self::LAST_PLANET] > $longitude1[$x + self::LAST_PLANET + 1]) {

                    if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[$x + self::LAST_PLANET + 1] && $pl >= 0)) {

                        $house_pos1[$y] = $x;
                        continue;

                    }

                }

                if ($x == 12 && ($longitude1[$x + self::LAST_PLANET] > $longitude1[self::LAST_PLANET + 1])) {

                    if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[self::LAST_PLANET + 1] && $pl >= 0)) {
                        $house_pos1[$y] = $x;
                    }

                    continue;
                }

                if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[$x + self::LAST_PLANET + 1]) && ($x < 12)) {

                    $house_pos1[$y] = $x;
                    continue;
                }

                if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[self::LAST_PLANET + 1]) && ($x == 12)) {
                    $house_pos1[$y] = $x;
                }
            }
        }

        //display natal data
        $secs = "0";
        if ($timezone < 0) {
            $tz = $timezone;
        } else {
            $tz = "+" . $timezone;
        }

        $hr_ob = $hour;
        $min_ob = $minute;

        $ubt1 = 0;
        if (($hr_ob == 12) && ($min_ob == 0)) {
            $ubt1 = 1;                // this person has an unknown birth time
        }

        $ubt2 = $ubt1;

        $rx1 = "";
        for ($i = 0; $i <= self::SE_TNODE; $i++) {

            if ($speed1[$i] < 0) {
                $rx1 .= "R";
            } else {
                $rx1 .= " ";
            }
        }

        $rx2 = $rx1;

        for ($i = 1; $i <= self::LAST_PLANET; $i++) {
            $hc1[$i] = $longitude1[self::LAST_PLANET + $i];
        }

        if ($ubt1 == 1) {
            $a1 = self::SE_TNODE;
        } else {
            $a1 = self::LAST_PLANET;
        }

        $planets = array();

        for ($i = 0; $i <= $a1; $i++) {

            if ($ubt1 == 1) {
                $hse = "&nbsp;";
            } else {
                $hse = floor($house_pos1[$i]);
            }

            $planets[$pl_name[$i]] = array_merge($this->convertLongitudeToArray($longitude1[$i]), array('mid' => $this->mid($rx1, $i + 1, 1), 'house' => $hse));
        }

        if ($ubt1 == 0) {

            $houses = array();

            for ($i = self::LAST_PLANET + 1; $i <= self::LAST_PLANET + 12; $i++) {

                if ($i == self::LAST_PLANET + 1) {
                    $name = 'Ascendant';
                } elseif ($i == self::LAST_PLANET + 10) {
                    $name = 'MC (Midheaven)';
                } else {
                    $name = 'House ' . ($i - self::LAST_PLANET);
                }

                $houses[$name] = $this->convertLongitudeToArray($longitude1[$i]);
            }
        }

        $aspects = array();

        // include Ascendant and MC
        $longitude1[self::LAST_PLANET + 1] = $hc1[1];
        $longitude1[self::LAST_PLANET + 2] = $hc1[10];

        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "Midheaven";

        if ($ubt1 == 1) {
            $a1 = self::SE_TNODE;
        } else {
            $a1 = self::LAST_PLANET + 2;
        }

        /**
         * Setup Aspect Testing array
         */
        $aspectOrbs = array(
            'conjunction' => array(
                'sun' => 10,
                'moon' => 10,
                'mercury' => 10,
                'venus' => 10,
                'mars' => 10,
                'jupiter' => 10,
                'saturn' => 10,
                'neptune' => 10,
                'uranus' => 10,
                'pluto' => 10,
                'chiron' => 10,
                'true node' => 10
            ),
            'opposition' => array(
                'sun' => 10,
                'moon' => 10,
                'mercury' => 10,
                'venus' => 10,
                'mars' => 10,
                'jupiter' => 10,
                'saturn' => 10,
                'neptune' => 10,
                'uranus' => 10,
                'pluto' => 10,
                'chiron' => 10,
                'true node' => 10
            ),
            'square' => array(
                'sun' => 10,
                'moon' => 10,
                'mercury' => 10,
                'venus' => 10,
                'mars' => 10,
                'jupiter' => 10,
                'saturn' => 10,
                'neptune' => 10,
                'uranus' => 10,
                'pluto' => 10,
                'chiron' => 10,
                'true node' => 10
            ),
            'trine' => array(
                'sun' => 10,
                'moon' => 10,
                'mercury' => 10,
                'venus' => 10,
                'mars' => 10,
                'jupiter' => 10,
                'saturn' => 10,
                'neptune' => 10,
                'uranus' => 10,
                'pluto' => 10,
                'chiron' => 10,
                'true node' => 10
            ),
            'sextile' => array(
                'sun' => 6,
                'moon' => 6,
                'mercury' => 6,
                'venus' => 6,
                'mars' => 6,
                'jupiter' => 6,
                'saturn' => 6,
                'neptune' => 6,
                'uranus' => 6,
                'pluto' => 6,
                'chiron' => 6,
                'true node' => 6
            ),
            'semisextile' => array(
                'sun' => 3,
                'moon' => 3,
                'mercury' => 3,
                'venus' => 3,
                'mars' => 3,
                'jupiter' => 3,
                'saturn' => 3,
                'neptune' => 3,
                'uranus' => 3,
                'pluto' => 3,
                'chiron' => 3,
                'true node' => 3
            ),
            'quincunx' => array(
                'sun' => 3,
                'moon' => 3,
                'mercury' => 3,
                'venus' => 3,
                'mars' => 3,
                'jupiter' => 3,
                'saturn' => 3,
                'neptune' => 3,
                'uranus' => 3,
                'pluto' => 3,
                'chiron' => 3,
                'true node' => 3
            ),
            'semisquare' => array(
                'sun' => 3,
                'moon' => 3,
                'mercury' => 3,
                'venus' => 3,
                'mars' => 3,
                'jupiter' => 3,
                'saturn' => 3,
                'neptune' => 3,
                'uranus' => 3,
                'pluto' => 3,
                'chiron' => 3,
                'true node' => 3
            ),
            'sesquiquare' => array(
                'sun' => 3,
                'moon' => 3,
                'mercury' => 3,
                'venus' => 3,
                'mars' => 3,
                'jupiter' => 3,
                'saturn' => 3,
                'neptune' => 3,
                'uranus' => 3,
                'pluto' => 3,
                'chiron' => 3,
                'true node' => 3
            ),
            'quintile' => array(
                'sun' => 1,
                'moon' => 1,
                'mercury' => 1,
                'venus' => 1,
                'mars' => 1,
                'jupiter' => 1,
                'saturn' => 1,
                'neptune' => 1,
                'uranus' => 1,
                'pluto' => 1,
                'chiron' => 1,
                'true node' => 1
            ),
            'biquintile' => array(
                'sun' => 1,
                'moon' => 1,
                'mercury' => 1,
                'venus' => 1,
                'mars' => 1,
                'jupiter' => 1,
                'saturn' => 1,
                'neptune' => 1,
                'uranus' => 1,
                'pluto' => 1,
                'chiron' => 1,
                'true node' => 1
            ),


        );

        $aspectAngles = array(
            'conjunction' => 0,
            'opposition' => 180,
            'square' => 90,
            'trine' => 120,
            'sextile' => 60,
            'semisextile' => 30,
            'quincunx' => 150,
            'semisquare' => 45,
            'sesquiquare' => 135,
            'quintile' => 72,
            'biquintile' => 144,
        );

        $asp_name[1] = "conjunction";
        $asp_name[2] = "opposition";
        $asp_name[3] = "trine";
        $asp_name[4] = "square";
        $asp_name[5] = "quincunx";
        $asp_name[6] = "sextile";
        /**
         * Aspects added by Ant
         */
        $asp_name[7] = 'semisquare';
        $asp_name[8] = 'semisextile';
        $asp_name[9] = 'sesquiquare';
        $asp_name[10] = 'quintile';
        $asp_name[11] = 'biquintile';

        $aspectNamesFlipped = array_flip($asp_name);

        /**
         * Calculate Aspects
         */
        for ($i = 0; $i <= $a1; $i++) {

            for ($j = 0; $j <= $a1; $j++) {

                $q = 0;
                $da = abs($longitude1[$i] - $longitude1[$j]);

                if ($da > 180) {
                    $da = 360 - $da;
                }

                /**
                 * Using: http://www.astro.com/astrology/in_aspect_e.htm as guideline
                 */

                foreach ($aspectAngles as $aspect => $absoluteAngle) {

                    // TODO: Throws warnings on $pl_name['vertex'] etc.
                    if (!isset($aspectOrbs[$aspect][strtolower($pl_name[$i])])) continue;
                    if (($da <= ($absoluteAngle + $aspectOrbs[$aspect][strtolower($pl_name[$i])])) && ($da >= ($absoluteAngle - $aspectOrbs[$aspect][strtolower($pl_name[$i])]))) {
                        $q = $aspectNamesFlipped[$aspect];
                        $dax = $da;
                    }

                }

                /**
                 * Add aspects to the aspects array
                 *
                 * Record Planet 1, Aspect, Planet 2 and the Orb.
                 */
                if ($q > 0 And $i != $j) {
                    $aspects[] = array('Planet1' => $pl_name[$i], 'Aspect' => ucwords($asp_name[$q]), 'Planet2' => $pl_name[$j], 'Orb' => sprintf("%.2f", abs($dax)));
                }
            }
        }

        $this->_planets = $planets;
        $this->_houses = $houses;
        $this->_aspects = $aspects;


        // echo "<pre>";
        // var_dump( $this->_aspects );
        // echo "</pre>";

    }

    public function drawChart($filename = null, $branded = false)
    {
        /**
         * Setup some variables we will re-use throughout this script
         */
//			размер картинки
        $this->chartWidth = 1200;
        $this->chartHeight = 1200;

        $this->output = [
            'chartWidth' => 1200,
            'chartHeight' => 1200,
        ];
        /**
         * Find Ascendant Offset
         *
         * This Determines at which angle we draw the chart
         */
        $ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
        $ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

        $this->ascendantDiff = $ascendantDiff;

        $this->output['ascendantDiff'] = $ascendantDiff;
        /**
         * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
         */
        foreach ($this->signOffset as $sign => $offset) {
            $newOffset = $offset + $ascendantDiff;
            if ($newOffset > 360) $newOffset -= 360;
            if ($newOffset <= 0) $newOffset += 360;

            $this->signOffset[$sign] = $newOffset;
        }

        $this->output['signOffset'] = $this->signOffset;

        /**
         * Create the Image as a PNG True Color and Fill it with White
         */
        //создаем холст
        $this->image = imageCreateTrueColor($this->chartWidth, $this->chartHeight);

        //рисуем прямоугольник
        imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, imagecolorallocate($this->image, 255, 255, 255));

        /**
         * Brand it if necessary
         */
        if ($branded) {
            $textBox = imagettfbbox(24, 45, $this->arialPath, $this->getFirstName());
            /*0	нижний левый угол, X координата
            1	нижний левый угол, Y координата
            2	нижний правый угол, X координата
            3	нижний правый угол, Y координата
            4	верхний правый угол, X координата
            5	верхний правый угол, Y координата
            6	верхний левый угол, X координата
            7	верхний левый угол, Y координата*/

            //ширина текста
            $textWidth = abs($textBox[4] - $textBox[0]);
            //высота текста
            $textHeight = abs($textBox[5] - $textBox[1]);

            //Рисуем имя
            imagettftext(
                $this->image,
                24,
                45,
                $this->getPolarX(315, 685) - ($textWidth / 2),
                $this->getPolarY(315, 685) + ($textHeight / 2),
                imagecolorallocate($this->image, 0, 0, 0),
                $this->arialPath,
                $this->getFirstName());

            $output['brandName'] = [

            ];

            //Рисуем дату рождения
            $textBox = imagettfbbox(18, 45, $this->arialPath, $this->getBirthDate('h:i A F j, Y'));
            $textWidth = abs($textBox[4] - $textBox[0]);
            $textHeight = abs($textBox[5] - $textBox[1]);

            imagettftext(
                $this->image,
                18,
                45,
                $this->getPolarX(315, 645) - ($textWidth / 2),
                $this->getPolarY(315, 645) + ($textHeight / 2),
                imagecolorallocate($this->image, 0, 0, 0),
                $this->arialPath,
                $this->getBirthDate('h:i A F j, Y'));


        }

        $this->output['firstName'] = $this->getFirstName();
        $this->output['birthDate'] = $this->getBirthDate('h:i A F j, Y');

        /**
         * Draw the Circles and Background Colors which everything will sit on top of
         */
        //внемний круг
        $this->drawCenteredCircle($this->acMcNotchOuterRadius, $this->notchBackgroundColor);
        //черный круг с зеленым фоном
        $this->drawCenteredCircle($this->starSignOuterRadius, $this->starSignBackgroundColor);
        //черный круг с белым фоном
        $this->drawCenteredCircle($this->starSignNotchOuterRadius, $this->notchBackgroundColor);
        //черный круг с синим фоном
        $this->drawCenteredCircle($this->planetOuterRadius, $this->planetBackgroundColor);
        //черный круг с белым фоном
        $this->drawCenteredCircle($this->aspectOuterRadius, $this->aspectBackgroundColor);
        //черный круг с белым фоном
        $this->drawCenteredCircle($this->centerOuterRadius, $this->centerBackgroundColor);

        $this->output['acMcNotchOuterRadius'] = $this->acMcNotchOuterRadius;
        $this->output['starSignOuterRadius'] = $this->starSignOuterRadius;
        $this->output['starSignNotchOuterRadius'] = $this->starSignNotchOuterRadius;
        $this->output['planetOuterRadius'] = $this->planetOuterRadius;
        $this->output['aspectOuterRadius'] = $this->aspectOuterRadius;
        $this->output['centerOuterRadius'] = $this->centerOuterRadius;
        /**
         * Draw the Notches in the Notch Areas
         *
         * Small notches 1 degree apart, Second Notches 5 degrees apart
         */
        //Насечки на кругах
        for ($i = 0; $i <= 360; $i += 1) {
            $newOffset = $i + $ascendantDiff;
            if ($newOffset > 360) $newOffset -= 360;
            if ($newOffset <= 0) $newOffset += 360;

            if (($i % 30) == 0) continue;

            // When drawing the notches on the outer circle we need to take into account the circles outer stroke...
            if (($i % 5) == 0) {

                $this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize2, $this->notchSize2, $this->notchColor);
                $this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize2, $this->notchColor);

            } else {

                $this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize1, $this->notchSize1, $this->notchColor);
                $this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize1, $this->notchColor);

            }

        }

        $this->output['notchSize2'] = $this->notchSize2;
        $this->output['notchSize1'] = $this->notchSize1;
        $this->output['circleStroke'] = $this->circleStroke;

        /**
         * Draw Star Sign Dividers
         */
        //Рисуем крупные насечки между зодиаками
        foreach ($this->signOffset as $i) {
            $this->drawPartialPolarLine($i, $this->planetOuterRadius, $this->acMcNotchOuterRadius - $this->planetOuterRadius, $this->notchColor);
        }

        /**
         * Draw Star Sign Glyphs
         */
        //Рисуем иконки знаки зодиаков
        foreach ($this->signOffset as $sign => $offset) {
            $this->drawCenteredText(
                $offset + $this->starSignSlice / 2,
                $this->starSignOuterRadius - ($this->starSignOuterRadius - $this->starSignNotchOuterRadius) / 2,
                $this->hamburgSymbolsPath,
                $this->signFont[$sign],
                $this->starSignSize,
                $this->starSignColor
            );
        }

        $this->output['starSignSlice'] = $this->starSignSlice;
        $this->output['hamburgSymbolsPath'] = $this->hamburgSymbolsPath;
        $this->output['starSignSize'] = $this->starSignSize;
        $this->output['starSignColor'] = $this->starSignColor;
        $this->output['signFont'] = $this->signFont;

        /**
         * Draw the Planet Notches to Show Where they Occur
         *
         * NOTE: We also make a note of where they are being plotted so we can do draw the aspects later.
         */
        $this->output['planets'] = $this->_planets;
        $this->output['ignorePlanets'] = $this->ignorePlanets;
        $this->output['starSignNotchSize'] = $this->starSignNotchSize;
        foreach ($this->_planets as $planet => $details) {

            // Skip over planets we want to ignore
            if (in_array($planet, $this->ignorePlanets)) continue;

            // Find the angle of the sign
            $planetRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
            $planetAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $planetRelativeAngle);

            // draw a line to mark its place
            //Рисуем насечку на каждую планету
            $this->drawPartialPolarLine(
                $planetAbsoluteAngle,
                $this->starSignNotchOuterRadius - $this->starSignNotchSize,
                $this->starSignNotchSize,
                $this->starSignNotchColor
            );

            // Record the Planet Positions for Drawing Aspects... Record the Position at the Outer Radius of the Aspect Circle
            $this->planetPositions[$planet]['x'] = $this->getPolarX($planetAbsoluteAngle, $this->aspectOuterRadius);
            $this->planetPositions[$planet]['y'] = $this->getPolarY($planetAbsoluteAngle, $this->aspectOuterRadius);

            // Record the Angles so we can later do Collision Detection and Draw the Glyphs in the right spot
            $this->planetAngles[$planet] = $planetAbsoluteAngle;
            $this->planetGlyphAngles[$planet] = $planetAbsoluteAngle;
        }

        $this->output['planetPositions'] = $this->planetPositions;
        $this->output['planetAngles'] = $this->planetAngles;
        $this->output['aspects'] = $this->_aspects;
        $this->output['ignoreAspects'] = $this->ignoreAspects;
        $this->output['aspectStyles'] = $this->aspectStyles;

        /**
         * Draw in the Aspects
         */
        foreach ($this->_aspects as $aspect) {

            if ((in_array($aspect['Planet1'], $this->ignoreAspects)) || (in_array($aspect['Planet2'], $this->ignoreAspects))) continue;

            if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'line') {
                //Рисуем цветные линии
                AdGuru_Image_Toolbox::imageSmoothAlphaLine(
                    $this->image,
                    $this->planetPositions[$aspect['Planet1']]['x'],
                    $this->planetPositions[$aspect['Planet1']]['y'],
                    $this->planetPositions[$aspect['Planet2']]['x'],
                    $this->planetPositions[$aspect['Planet2']]['y'],
                    $this->aspectStyles[$aspect['Aspect']]['color']['R'], $this->aspectStyles[$aspect['Aspect']]['color']['G'], $this->aspectStyles[$aspect['Aspect']]['color']['B'], 0);

            } else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dashed') {
                //Рисуем пунктирные линии
                $aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
                $aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
                $white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
                $style = array($aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white, $white);
                imagesetstyle($this->image, $style);
                imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

            } else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dotted') {
                //Рисуем точечные линии
                $aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
                $aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
                $white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
                $style = array($aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white);
                imagesetstyle($this->image, $style);
                imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

            }

        }

        /* Display House Positions, MC and Ascendant
        *
        * TODO: Add thickness to antialiased lines..
        */
        $houseCounter = 1;
        $accumulatedAngel = 0;
        $ascendantAngel = 0;

        $this->output['houseCounter'] = $houseCounter;
        $this->output['accumulatedAngel'] = $accumulatedAngel;
        $this->output['ascendantAngel'] = $ascendantAngel;
        $this->output['houses'] = $this->_houses;
        $this->output['acMcNotchSize'] = $this->acMcNotchSize;
        $this->output['starSignNotchOuterRadius'] = $this->starSignNotchOuterRadius;
        $this->output['acMcGlyphDistance'] = $this->acMcGlyphDistance;
        $this->output['acMcTextOffset'] = $this->acMcTextOffset;
        $this->output['acMcTextDistance'] = $this->acMcTextDistance;
        $this->output['acMcGlyphFontSize'] = $this->acMcGlyphFontSize;
        $this->output['acMcTextFontSize'] = $this->acMcTextFontSize;

        foreach ($this->_houses as $house => $details) {

            // Find the angle of the house
            $houseAbsoluteAngle = (($houseCounter - 1) * 30) + 270;

            // 30 Deg Houses
            if ($houseAbsoluteAngle >= 0) {
                $houseAbsoluteAngle = (((30 - ($houseAbsoluteAngle % 30)) >= 15) ? ($houseAbsoluteAngle - ($houseAbsoluteAngle % 30)) : ($houseAbsoluteAngle + (30 - ($houseAbsoluteAngle % 30)))) + 0.001;
            } else {
                $houseAbsoluteAngle = (((-30 - ($houseAbsoluteAngle % (-30))) >= -15) ? ($houseAbsoluteAngle + (-30) - ($houseAbsoluteAngle % (-30))) : ($houseAbsoluteAngle - ($houseAbsoluteAngle % (-30)))) - 0.001;
            }

            if ($house == 'Ascendant') {
                $ascendantAngel = $houseAbsoluteAngle + 0.001;
            } else {
                $houseAbsoluteAngle = $ascendantAngel + $accumulatedAngel + 0.001;
            }

            // echo "<pre>";
            // var_dump($houseAbsoluteAngle);
            // echo "</pre><br>";

            // Draw the Ascendant, Descendant, MC (MidHeaven) and IC

            //Рисуем линии от центра
            if (($house == 'MC (Midheaven)') || ($house == 'Ascendant')) {

                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
                $this->drawCenteredText($houseAbsoluteAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($house)], $this->acMcGlyphFontSize, $this->acMcColor);
                $this->drawCenteredText($houseAbsoluteAngle - $this->acMcTextOffset, $this->acMcTextDistance, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

                $oppAngle = $houseAbsoluteAngle - 180;
                if ($oppAngle <= 0) $oppAngle += 360;
                $oppGlyph = ($house == 'Ascendant' ? $this->signFont['descendant'] : $this->signFont['ic']);

                $this->drawCenteredText($oppAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $oppGlyph, $this->acMcGlyphFontSize, $this->acMcColor);
                $this->drawCenteredText($oppAngle - $this->acMcTextOffset, $this->acMcTextDistance, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

                // Record House Position for Plotting Aspects...
                $planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
                $this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
                $this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


                // Draw Standard House Divider
            } else {
                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->notchColor);
            }

            // Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
            $this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

            $houseCounter++;

            $accumulatedAngel -= 30;
        }

        /**
         * Draw the Numbers in between the House Lines now that we have each House Angle.
         */
        //Рисуем цыфры в центре круга
        foreach ($this->houseAngles as $houseNumber => $currentAngle) {

            $nextAngle = ($houseNumber < 12 ? $this->houseAngles[$houseNumber + 1] : $this->houseAngles[1]);
            $diff = ($currentAngle > $nextAngle ? $currentAngle - $nextAngle : ($currentAngle + 360) - $nextAngle);

            $newAngle = $currentAngle - ($diff / 2);

            if ($newAngle <= 0) {
                $newAngle += 360;
            }

            $this->drawCenteredText($newAngle, $this->houseTextDistance, $this->arialPath, $houseNumber, $this->houseTextFontSize, $this->notchColor);
        }

        $this->output['houseAngles'] = $this->houseAngles;
        $this->output['houseTextDistance'] = $this->houseTextDistance;
        $this->output['houseTextFontSize'] = $this->houseTextFontSize;


        asort($this->planetGlyphAngles);

        $kSortedPlanets = array();
        foreach ($this->planetGlyphAngles as $planet => $angle) {
            $kSortedPlanets[] = array('planet' => $planet, 'angle' => $angle);
        }

        $this->kSortedPlanets = $kSortedPlanets;
        $this->copyKSortedPlanets = $kSortedPlanets;

        if (!isset($isCreateVideo)) {
            $planetCollisionLimit = $this->planetCollisionLimit;
        } else {
            $planetCollisionLimit = $this->videoPlanetCollisionLimit;
        }

        $this->kSortedPlanets[12] =
            array(
                "planet" => "MC",
                "angle" => 0
            );

        $this->kSortedPlanets[13] =
            array(
                "planet" => "DC",
                "angle" => 90
            );

        $this->kSortedPlanets[14] =
            array(
                "planet" => "IC",
                "angle" => 180
            );

        $this->kSortedPlanets[15] =
            array(
                "planet" => "AC",
                "angle" => 270
            );

        $planetsNames = array();
        $planetsAngels = array();

        foreach ($this->kSortedPlanets as $planetKey => $planetInfo) {
            $planetName = $planetInfo["planet"];
            $planetAngel = $planetInfo["angle"];

            array_push($planetsNames, $planetName);
            array_push($planetsAngels, $planetAngel);
        }

        array_multisort(
            $planetsAngels,
            $planetsNames
        );

        $sortedPlanets = array();

        foreach ($planetsNames as $planetKey => $planetName) {
            foreach ($this->kSortedPlanets as $planetKey => $planetInfo) {
                $initPlanetName = $planetInfo["planet"];
                $initPlanetAngel = $planetInfo["angle"];

                if ($initPlanetName == $planetName) {
                    $initPlanetKey = $planetKey;

                    break;
                }
            }

            array_push($sortedPlanets,
                array(
                    "planet" => $planetName,
                    "angle" => $this->kSortedPlanets[$initPlanetKey]["angle"]
                )
            );
        }

        $previousAngel = 0;

        foreach ($sortedPlanets as $planetKey => $planetInfo) {
            $planetName = $planetInfo["planet"];
            $currentAngel = $planetInfo["angle"];

            if (($currentAngel - $previousAngel) < $planetCollisionLimit) {
                $currentAngel = $previousAngel + $planetCollisionLimit;
            }

            switch ($planetName) {
                case 'MC':
                case 'DC':
                case 'IC':
                case 'AC':
                    unset($sortedPlanets[$planetKey]);

                default:
                    $sortedPlanets[$planetKey]["angle"] = $currentAngel;
            }

            $previousAngel = $currentAngel;
        }

        $kSortedPlanets = $sortedPlanets;
        $this->kSortedPlanets = $kSortedPlanets;

        /**
         * Draw the Planet Glyphs now that they've been adjusted.
         */
        //Рисуем планеты
        foreach (array_slice($kSortedPlanets, 0, 12) as $planetDetails) {
            $this->drawCenteredText($planetDetails['angle'], $this->planetGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($planetDetails['planet'])], $this->planetGlyphFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetDegTextDistance, $this->arialPath, $this->_planets[$planetDetails['planet']]['deg'], $this->planetTextFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetSignGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($this->_planets[$planetDetails['planet']]['sign'])], $this->planetSignGlyphFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetMinTextDistance, $this->arialPath, $this->_planets[$planetDetails['planet']]['min'], $this->planetTextFontSize, $this->planetColor);
        }

        $this->output['kSortedPlanets'] = array_slice($kSortedPlanets, 0, 12);
        $this->output['planetGlyphDistance'] = $this->planetGlyphDistance;
        $this->output['planetDegTextDistance'] = $this->planetDegTextDistance;
        $this->output['planetSignGlyphDistance'] = $this->planetSignGlyphDistance;
        $this->output['planetMinTextDistance'] = $this->planetMinTextDistance;
        $this->output['planetGlyphFontSize'] = $this->planetGlyphFontSize;
        $this->output['planetTextFontSize'] = $this->planetTextFontSize;
        $this->output['planetSignGlyphFontSize'] = $this->planetSignGlyphFontSize;

        //header( 'Content-Type: image/png' );
        if (is_null($filename)) {
            $filename = 'chart-' . time() . rand(1, 20) . '.png';
            imagejpeg($this->image, $this->tempImagePath . $filename);
        } else {
            imagepng($this->image, $this->tempImagePath . $filename);
        }

        return $filename;
    }

    public function initializeChart()
    {
        $this->chartWidth = 1200;
        $this->chartHeight = $this->chartWidth;

        $this->hamburgSymbolsPath = Config::FONTS_PATH . 'HamburgSymbols.ttf';
        $this->arialPath = Config::FONTS_PATH . 'arial.ttf';

        $this->tempImagePath = Config::TEMP_PATH;

        $this->acMcOuterRadius = $this->chartWidth / 2;
        $this->acMcNotchOuterRadius = $this->acMcOuterRadius - 50;
        $this->starSignOuterRadius = $this->acMcNotchOuterRadius - ($this->notchSize2 * 2);
        $this->starSignNotchOuterRadius = $this->starSignOuterRadius - 100;
        $this->planetOuterRadius = $this->starSignNotchOuterRadius - ($this->notchSize2 * 2);
        $this->aspectOuterRadius = $this->planetOuterRadius - 150;
        $this->centerOuterRadius = 50;

        $this->acMcBackgroundColor = array('R' => 0xCD, 'G' => 0xE3, 'B' => 0xCA);
        $this->starSignBackgroundColor = array('R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6);
        $this->planetBackgroundColor = array('R' => 0xCA, 'G' => 0xCE, 'B' => 0xE3);
        $this->aspectBackgroundColor = array('R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF);
        $this->centerBackgroundColor = array('R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF);
        $this->notchBackgroundColor = array('R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF);
        $this->starSignNotchColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF);
        $this->notchColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00);
        $this->strokeColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00);

        $this->starSignColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00);
        $this->planetColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00);
        $this->acMcColor = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00);

        $this->ignorePlanets = array('Lilith', 'P. of Fortune', 'Vertex');
        $this->ignoreAspects = array('Ascendant', 'Chiron', 'Midheaven', 'Lilith', 'P. of Fortune', 'Vertex', 'True Node');

        $this->aspectStyles = array(
            'Square' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Trine' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Conjunction' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Quincunx' => array('style' => 'dashed', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
            'Sextile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Semisextile' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
            'Semisquare' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
            'Opposition' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Sesquiquare' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Quintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
            'Biquintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
        );

        $this->acMcGlyphDistance = $this->acMcOuterRadius - 20;
        $this->acMcTextDistance = $this->acMcOuterRadius - 25;

        $this->houseTextDistance = $this->centerOuterRadius + 25;

        $this->planetGlyphDistance = $this->planetOuterRadius - 35;
        $this->planetMinTextDistance = $this->planetGlyphDistance - 35;
        $this->planetSignGlyphDistance = $this->planetMinTextDistance - 25;
        $this->planetDegTextDistance = $this->planetSignGlyphDistance - 25;
    }

    /**
     * Return Birth Date
     *
     * @param String $format
     * @return Date
     */
    public function getBirthDate($format = null)
    {
        if (!($this->_birthDate instanceof DateTime) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date'])) && (isset($this->_data['birth_time']) && !empty($this->_data['birth_time']))) {
            $this->_birthDate = new DateTime($this->_data['birth_date']['year'] . '-' . $this->_data['birth_date']['month'] . '-' . $this->_data['birth_date']['day'] . ' ' . $this->_data['birth_time']['hour'] . ':' . $this->_data['birth_time']['minute'] . ' ' . $this->_data['birth_time']['meridiem']);
        } else if (!($this->_birthDate instanceof DateTime) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date']))) {
            $this->_birthDate = new DateTime($this->_data['birth_date']);
        }

        if ($this->_birthDate instanceof DateTime) {

            if (is_null($format))
                return $this->_birthDate;

            return $this->_birthDate->format($format);
        }

        return false;
    }

    /**
     * Return Birth Date
     *
     * @param String $format
     * @return Date
     */
    public function setBirthDate($timestamp = null)
    {
        if ($this->_birthDate instanceof DateTime) {

            if (is_null($timestamp))
                return false;

            return $this->_birthDate->setTimestamp($timestamp);
        }

        return false;
    }

    /**
     * Return Location
     *
     * @return Acs_Location_Row
     */
    public function getLocationRow()
    {

        if (!$this->_location) {

            $locationTable = new Acs_Location_Table();
            $row = $locationTable->getLocationRow($this->_data['acs_location_id']);

            if ($row)
                $this->_location = $row;
        }

        if ($this->_location)
            return $this->_location;

        return null;

    }

    /**
     * Return Timezone
     *
     * @return Acs_Timezone
     */
    public function getTimezone()
    {

        if (!$this->_timezone instanceof Acs_Timezone) {

            $tz = new Acs_Timezone($this->getLocationRow(), $this->getBirthDate());

            if ($tz instanceof Acs_Timezone)
                $this->_timezone = $tz;
        }

        if ($this->_timezone instanceof Acs_Timezone)
            return $this->_timezone;

        return null;

    }

    /**
     * Set data field value
     *
     * @param  string $columnName The column key.
     * @param  mixed $value The value for the property.
     * @return void
     * @throws Exception
     */
    public function __set($columnName, $value)
    {
        if (!array_key_exists($columnName, $this->_data)) {
            throw new Exception("Specified column \"$columnName\" is not in the row");
        }
        $this->_data[$columnName] = $value;
    }

    /**
     * Prepare variable to the date function use
     *
     * @param  mixed $integer The varible.
     * @return string
     */
    public static function convertIntToStr($integer)
    {
        $string = ( string )$integer;

        $firstCharacter = $string[0];
        $zeroCharacter = "0";

        if ((strlen($string) == 1) and ($firstCharacter != $zeroCharacter)) {
            $string = "0" . $string;
        }

        return $string;
    }

    /**
     * Find a Zodiac Sign by Birthdate
     *
     * @param  mixed $birthdate $year-$month-$day.
     * @return string
     */
    public static function getZodiac($birthdate)
    {

        $zodiac = "";

        list ($year, $month, $day) = explode("-", $birthdate);

        if (($month == 3 && $day > 20) || ($month == 4 && $day < 20)) {
            $zodiac = "Aries";
        } elseif (($month == 4 && $day > 19) || ($month == 5 && $day < 21)) {
            $zodiac = "Taurus";
        } elseif (($month == 5 && $day > 20) || ($month == 6 && $day < 21)) {
            $zodiac = "Gemini";
        } elseif (($month == 6 && $day > 20) || ($month == 7 && $day < 23)) {
            $zodiac = "Cancer";
        } elseif (($month == 7 && $day > 22) || ($month == 8 && $day < 23)) {
            $zodiac = "Leo";
        } elseif (($month == 8 && $day > 22) || ($month == 9 && $day < 23)) {
            $zodiac = "Virgo";
        } elseif (($month == 9 && $day > 22) || ($month == 10 && $day < 23)) {
            $zodiac = "Libra";
        } elseif (($month == 10 && $day > 22) || ($month == 11 && $day < 22)) {
            $zodiac = "Scorpio";
        } elseif (($month == 11 && $day > 21) || ($month == 12 && $day < 22)) {
            $zodiac = "Sagittarius";
        } elseif (($month == 12 && $day > 21) || ($month == 1 && $day < 20)) {
            $zodiac = "Capricorn";
        } elseif (($month == 1 && $day > 19) || ($month == 2 && $day < 19)) {
            $zodiac = "Aquarius";
        } elseif (($month == 2 && $day > 18) || ($month == 3 && $day < 21)) {
            $zodiac = "Pisces";
        }

        return $zodiac;
    }
}

?>