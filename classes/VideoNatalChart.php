<?php
	class VideoNatalChart extends AdaptedNatalChart {
		protected $houseTextFontSize = 24;
		protected $acMcGlyphFontSize = 30;
		protected $acMcTextFontSize = 20;
		protected $acMcColorActive;
		
		public function drawCenteredCircle($radius, $color, $stroke = 1) {
			for( $counter = 0; $counter < $stroke; $counter++ ) {
				AdaptedToolbox::imageSmoothCircle( $this->image, $this->chartWidth/2,  $this->chartHeight/2,  $radius+$counter, $color);
			}
		}
		
		function drawPartialPolarLine($angle, $start, $length, $color) {

			$angle = ($angle == 0 ? 360 : $angle);

			$startx = $this->chartWidth/2 + sin( deg2rad( $angle ) )  * $start;
			$starty = $this->chartWidth/2 + cos( deg2rad( $angle + 180 ) ) * $start;

			$endx = $startx + sin( deg2rad( $angle ) )  * $length;
			$endy = $starty + cos( deg2rad( $angle + 180 ) ) * $length;

			if ( ($angle == 90) || ($angle == 180) || ($angle == 270) || ($angle == 360) ) {
				imageline($this->image, $startx, $starty, $endx, $endy, $color);
			} else {
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty, $endx, $endy, $color['R'], $color['G'], $color['B'], 0);
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty-1, $endx, $endy-1, $color['R'], $color['G'], $color['B'], 0);
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty-2, $endx, $endy-2, $color['R'], $color['G'], $color['B'], 0);
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty-3, $endx, $endy-3, $color['R'], $color['G'], $color['B'], 0);
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty-4, $endx, $endy-4, $color['R'], $color['G'], $color['B'], 0);
				AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty-5, $endx, $endy-5, $color['R'], $color['G'], $color['B'], 0);				
			}


		}		
	
		public function drawOuterCircle($filename = null, $branded = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */ 
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			/**
			 * Draw the Circles and Background Colors which everything will sit on top of
			 */
			$stroke = 20;
			
			$this->drawCenteredCircle($this->acMcNotchOuterRadius, $this->notchBackgroundColor, $stroke);
			$this->drawCenteredCircle($this->starSignOuterRadius, $this->starSignBackgroundColor, $stroke);
			//$this->drawCenteredCircle($this->starSignNotchOuterRadius, $this->notchBackgroundColor, $stroke);
			//$this->drawCenteredCircle($this->planetOuterRadius, $this->planetBackgroundColor, $stroke);
			//$this->drawCenteredCircle($this->aspectOuterRadius, $this->aspectBackgroundColor, $stroke);
			//$this->drawCenteredCircle($this->centerOuterRadius, $this->centerBackgroundColor, $stroke);

			/**
			 * Draw the Notches in the Notch Areas
			 *
			 * Small notches 1 degree apart, Second Notches 5 degrees apart
			 */
			for ($i = 0; $i <= 360; $i += 1) {
				$newOffset = $i + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				if ( ($i % 30) == 0 ) continue;

				// When drawing the notches on the outer circle we need to take into account the circles outer stroke...
				if ( ($i % 5) == 0 ) {

					//$this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize2, $this->notchSize2, $this->notchColor);
					$this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize2, $this->notchColor);

				} else {

					//$this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize1, $this->notchSize1, $this->notchColor);
					$this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize1, $this->notchColor);

				}

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}	

		public function drawInnerCircle($filename = null, $branded = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */ 
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			/**
			 * Brand it if necessary
			 */
			if($branded) {

				$textBox = imagettfbbox(24,45,$this->arialPath, $this->getFirstName());
				$textWidth = abs($textBox[4] - $textBox[0]);
				$textHeight = abs($textBox[5] - $textBox[1]);
				
				imagettftext(
					$this->image,
					24,
					45,
					$this->getPolarX(315, 685) - ($textWidth/2),
					$this->getPolarY(315, 685) + ($textHeight/2),
					imagecolorallocate($this->image, 0, 0, 0),
					$this->arialPath,
					$this->getFirstName());
				
					//date('g:i a \o\n l \t\h\e jS \of F Y', mktime($hour, $minute, 0, $month, $day, $year))

				$textBox = imagettfbbox(18,45,$this->arialPath, $this->getBirthDate('h:i A F j, Y'));
				$textWidth = abs($textBox[4] - $textBox[0]);
				$textHeight = abs($textBox[5] - $textBox[1]);
				
				imagettftext(
					$this->image,
					18,
					45,
					$this->getPolarX(315, 645) - ($textWidth/2),
					$this->getPolarY(315, 645) + ($textHeight/2),
					imagecolorallocate($this->image, 0, 0, 0),
					$this->arialPath,
					$this->getBirthDate('h:i A F j, Y'));
					
					
					
			}

			/**
			 * Draw the Circles and Background Colors which everything will sit on top of
			 */
			$stroke = 20;
			
			//$this->drawCenteredCircle($this->acMcNotchOuterRadius, $this->notchBackgroundColor, $stroke);
			//$this->drawCenteredCircle($this->starSignOuterRadius, $this->starSignBackgroundColor, $stroke);
			
			$this->drawCenteredCircle($this->starSignNotchOuterRadius, $this->notchBackgroundColor, $stroke);
			$this->drawCenteredCircle($this->planetOuterRadius, $this->planetBackgroundColor, $stroke);
			$this->drawCenteredCircle($this->aspectOuterRadius, $this->aspectBackgroundColor, $stroke);
			$this->drawCenteredCircle($this->centerOuterRadius, $this->centerBackgroundColor, $stroke);

			/**
			 * Draw the Notches in the Notch Areas
			 *
			 * Small notches 1 degree apart, Second Notches 5 degrees apart
			 */
			for ($i = 0; $i <= 360; $i += 1) {
				$newOffset = $i + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				if ( ($i % 30) == 0 ) continue;

				// When drawing the notches on the outer circle we need to take into account the circles outer stroke...
				if ( ($i % 5) == 0 ) {

					$this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize2, $this->notchSize2, $this->notchColor);
					$this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize2, $this->notchColor);

				} else {

					$this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize1, $this->notchSize1, $this->notchColor);
					$this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize1, $this->notchColor);

				}

			}
			
			/**
			 * Draw Star Sign Dividers
			 */
			foreach ($this->signOffset as $i) {
				$this->drawPartialPolarLine($i, $this->planetOuterRadius+10, $this->acMcNotchOuterRadius - $this->planetOuterRadius, $this->notchColor);
			}			

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}	

		public function drawHouses($filename = null, $branded = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			// echo "<pre>";				
			// var_dump($this->_houses);
			// echo "</pre>";			
			
			/**
			 * Display House Positions, MC and Ascendant
			 *
			 * TODO: Add thickness to antialiased lines..
			 */
			$houseCounter = 1;
			$accumulatedAngel = 0;
			$ascendantAngel = 0;
			
			foreach ($this->_houses as $house => $details) {				
				// echo "<pre>";				
				// var_dump($details);
				// echo "</pre>";
				
				// Find the angle of the house
				$houseRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
				$houseAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $houseRelativeAngle - 250 + $this->planetGlyphAngles[ "Sun" ]);				
				
				// echo "$house";
				// echo "<pre>";				
				// var_dump($houseRelativeAngle);
				// var_dump($houseAbsoluteAngle);
				// echo "</pre>";	

				if ( ($house != 'MC (Midheaven)') and ($house != 'Ascendant') ) {
					// 30 Deg Houses
					if( $houseAbsoluteAngle >= 0) {
						$houseAbsoluteAngle = ( ( ( 30 - ( $houseAbsoluteAngle % 30 ) ) >= 15 ) ? ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % 30 ) ) : ( $houseAbsoluteAngle + ( 30 - ( $houseAbsoluteAngle % 30 ) ) ) );
					}
					else {
						$houseAbsoluteAngle = ( ( ( -30 - ( $houseAbsoluteAngle % ( -30 ) ) ) >= -15 ) ? ( $houseAbsoluteAngle + ( -30 ) - ( $houseAbsoluteAngle % ( -30 ) ) ) : ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % ( -30 ) ) ) );
					}
				}
				else if ( $house == 'MC (Midheaven)' ) {
					$houseAbsoluteAngle = -239.9;
				}				
				
				if ( $house == 'Ascendant' ) {
					$houseAbsoluteAngle = 29.9;
					$ascendantAngel = $houseAbsoluteAngle;
				}				
				else if ( ( strval( $house ) !== 'House 11' ) and ( strval( $house ) !== 'House 12' ) ) {//echo "@$house<br>";
					$houseAbsoluteAngle = $ascendantAngel + $accumulatedAngel;
				}
				
				// echo "<pre>";		
				// var_dump($houseAbsoluteAngle);
				// echo "</pre><br>";				
				
				// Draw the Ascendant, Descendant, MC (MidHeaven) and IC
				if ( ($house == 'MC (Midheaven)') || ($house == 'Ascendant') ) {

					// Record House Position for Plotting Aspects...
					$planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
					$this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
					$this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


				// Draw Standard House Divider
				}
				
				$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->notchColor);
			

				// Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
				$this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

				$houseCounter++;

				$accumulatedAngel -= 30;
			}

			// echo "<pre>";				
			// var_dump($this->houseAngles);
			// echo "</pre>";

			/**
			 * Draw the Numbers in between the House Lines now that we have each House Angle.
			 */
			foreach ($this->houseAngles as $houseNumber => $currentAngle) {

				$nextAngle = ($houseNumber < 12 ? $this->houseAngles[$houseNumber + 1] : $this->houseAngles[1]);
				$diff = ($currentAngle > $nextAngle ? $currentAngle - $nextAngle : ($currentAngle + 360) - $nextAngle);

				$newAngle = $currentAngle - ($diff / 2);

				if ($newAngle <= 0) {
					$newAngle += 360;
				}

				$this->drawCenteredText($newAngle, $this->houseTextDistance, $this->arialPath, $houseNumber, $this->houseTextFontSize, $this->notchColor);
			}

			//var_dump($houseCollisions);
			foreach ($houseCollisions as $houseNumber => $collisionArray) {
				asort($collisionArray);

				$counter = 0;
				foreach ($collisionArray as $planet => $planetAngle) {

					if ( ($counter == 0) && (sizeof($collisionArray) == 1)) {
						// Check to see if it is bordering on any house boundary... and if it is lock it
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house... lock it.<br />';
						$this->_planetsLocked[] = $planet;
					} else if ($counter == 0) {
						$tmpHouseNum = ($houseNumber == 12 ? 1 : $houseNumber + 1);
						$diff = ( ($planetAngle - $this->houseAngles[$tmpHouseNum]) < 0 ? ($planetAngle - $this->houseAngles[$tmpHouseNum]) * -1 : ($planetAngle - $this->houseAngles[$tmpHouseNum]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the first one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the first one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					} else if ( ($counter == sizeof($collisionArray) - 1) ) {
						$diff = ( ($planetAngle - $this->houseAngles[$houseNumber]) < 0 ? ($planetAngle - $this->houseAngles[$houseNumber]) * -1 : ($planetAngle - $this->houseAngles[$houseNumber]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the last one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the last one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					}

					$counter++;

				}

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}

		public function drawActiveHouse($filename = null, $houseNum = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			/**
			 * Display House Positions, MC and Ascendant
			 *
			 * TODO: Add thickness to antialiased lines..
			 */
			$houseCounter = 1;
			$accumulatedAngel = 0;
			$ascendantAngel = 0;
			
			foreach ($this->_houses as $house => $details) {				
				// echo "<pre>";				
				// var_dump($details);
				// echo "</pre>";
				
				// Find the angle of the house
				$houseRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
				$houseAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $houseRelativeAngle - 250 + $this->planetGlyphAngles[ "Sun" ]);				
				
				// echo "$house";
				// echo "<pre>";				
				// var_dump($houseRelativeAngle);
				// var_dump($houseAbsoluteAngle);
				// echo "</pre>";	

				if ( ($house != 'MC (Midheaven)') and ($house != 'Ascendant') ) {
					// 30 Deg Houses
					if( $houseAbsoluteAngle >= 0) {
						$houseAbsoluteAngle = ( ( ( 30 - ( $houseAbsoluteAngle % 30 ) ) >= 15 ) ? ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % 30 ) ) : ( $houseAbsoluteAngle + ( 30 - ( $houseAbsoluteAngle % 30 ) ) ) );
					}
					else {
						$houseAbsoluteAngle = ( ( ( -30 - ( $houseAbsoluteAngle % ( -30 ) ) ) >= -15 ) ? ( $houseAbsoluteAngle + ( -30 ) - ( $houseAbsoluteAngle % ( -30 ) ) ) : ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % ( -30 ) ) ) );
					}
				}
				else if ( $house == 'MC (Midheaven)' ) {
					$houseAbsoluteAngle = -239.9;
				}				
				
				if ( $house == 'Ascendant' ) {
					$houseAbsoluteAngle = 29.9;
					$ascendantAngel = $houseAbsoluteAngle;
				}				
				else if ( ( strval( $house ) !== 'House 11' ) and ( strval( $house ) !== 'House 12' ) ) {echo "@$house<br>";
					$houseAbsoluteAngle = $ascendantAngel + $accumulatedAngel;
				}
				
				// echo "<pre>";		
				// var_dump($houseAbsoluteAngle);
				// echo "</pre><br>";				
				
				// Draw the Ascendant, Descendant, MC (MidHeaven) and IC
				if ( ($house == 'MC (Midheaven)') || ($house == 'Ascendant') ) {

					// Record House Position for Plotting Aspects...
					$planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
					$this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
					$this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


				// Draw Standard House Divider
				}
				

				if( ( "House " . $houseNum ) == $house ) {
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);
				}
				else if( ( "House " . ($houseNum + 1) ) == $house ) {
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);
				}
				else if ( ($house == 'MC (Midheaven)') /*($house == 'Ascendant')*/ ) {
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);
					$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->acMcColorActive);					
				}				
			

				// Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
				$this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

				$houseCounter++;

				$accumulatedAngel -= 30;
			}

			// echo "<pre>";				
			// var_dump($this->houseAngles);
			// echo "</pre>";

			/**
			 * Draw the Numbers in between the House Lines now that we have each House Angle.
			 */
			foreach ($this->houseAngles as $houseNumber => $currentAngle) {

				$nextAngle = ($houseNumber < 12 ? $this->houseAngles[$houseNumber + 1] : $this->houseAngles[1]);
				$diff = ($currentAngle > $nextAngle ? $currentAngle - $nextAngle : ($currentAngle + 360) - $nextAngle);

				$newAngle = $currentAngle - ($diff / 2);

				if ($newAngle <= 0) {
					$newAngle += 360;
				}

				if( $houseNum == $houseNumber ) {
					$this->drawCenteredText($newAngle, $this->houseTextDistance, $this->arialPath, $houseNumber, $this->houseTextFontSize, $this->acMcColorActive);
				}
			}

			//var_dump($houseCollisions);
			foreach ($houseCollisions as $houseNumber => $collisionArray) {
				asort($collisionArray);

				$counter = 0;
				foreach ($collisionArray as $planet => $planetAngle) {

					if ( ($counter == 0) && (sizeof($collisionArray) == 1)) {
						// Check to see if it is bordering on any house boundary... and if it is lock it
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house... lock it.<br />';
						$this->_planetsLocked[] = $planet;
					} else if ($counter == 0) {
						$tmpHouseNum = ($houseNumber == 12 ? 1 : $houseNumber + 1);
						$diff = ( ($planetAngle - $this->houseAngles[$tmpHouseNum]) < 0 ? ($planetAngle - $this->houseAngles[$tmpHouseNum]) * -1 : ($planetAngle - $this->houseAngles[$tmpHouseNum]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the first one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the first one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					} else if ( ($counter == sizeof($collisionArray) - 1) ) {
						$diff = ( ($planetAngle - $this->houseAngles[$houseNumber]) < 0 ? ($planetAngle - $this->houseAngles[$houseNumber]) * -1 : ($planetAngle - $this->houseAngles[$houseNumber]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the last one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the last one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					}

					$counter++;

				}

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}		

		public function drawSides($filename = null, $branded = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			/**
			 * Display House Positions, MC and Ascendant
			 *
			 * TODO: Add thickness to antialiased lines..
			 */
			$houseCounter = 1;
			$accumulatedAngel = 0;
			$ascendantAngel = 0;
			
			foreach ($this->_houses as $house => $details) {				
				// echo "<pre>";				
				// var_dump($details);
				// echo "</pre>";
				
				// Find the angle of the house
				$houseRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
				$houseAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $houseRelativeAngle - 250 + $this->planetGlyphAngles[ "Sun" ]);				
				
				// echo "$house";
				// echo "<pre>";				
				// var_dump($houseRelativeAngle);
				// var_dump($houseAbsoluteAngle);
				// echo "</pre>";	

				// 30 Deg Houses
				if( $houseAbsoluteAngle >= 0) {
					$houseAbsoluteAngle = ( ( ( 30 - ( $houseAbsoluteAngle % 30 ) ) >= 15 ) ? ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % 30 ) ) : ( $houseAbsoluteAngle + ( 30 - ( $houseAbsoluteAngle % 30 ) ) ) );
				}
				else {
					$houseAbsoluteAngle = ( ( ( -30 - ( $houseAbsoluteAngle % ( -30 ) ) ) >= -15 ) ? ( $houseAbsoluteAngle + ( -30 ) - ( $houseAbsoluteAngle % ( -30 ) ) ) : ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % ( -30 ) ) ) );
				}
				
				if ( $house == 'Ascendant' ) {
					$ascendantAngel = $houseAbsoluteAngle;
				}				
				else {
					$houseAbsoluteAngle = $ascendantAngel + $accumulatedAngel;
				}
				
				// echo "<pre>";		
				// var_dump($houseAbsoluteAngle);
				// echo "</pre><br>";				
				
				// Draw the Ascendant, Descendant, MC (MidHeaven) and IC
				if ( ($house == 'MC (Midheaven)') || ($house == 'Ascendant') ) {

					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
					$this->drawCenteredText($houseAbsoluteAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($house)], $this->acMcGlyphFontSize, $this->acMcColor );
					$this->drawCenteredText($houseAbsoluteAngle - $this->acMcTextOffset, $this->acMcTextDistance-40, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

					$oppAngle = $houseAbsoluteAngle - 180;
					if ($oppAngle <= 0) $oppAngle += 360;
					$oppGlyph = ($house == 'Ascendant' ? $this->signFont['descendant'] : $this->signFont['ic']);

					// Not sure why or how this works while it's commented out... but it does, otherwise we get a thick line for the DC.
					/*
					$this->drawPartialPolarLine($oppAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
					$this->drawPartialPolarLine($oppAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
					*/
					$this->drawCenteredText($oppAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $oppGlyph, $this->acMcGlyphFontSize, $this->acMcColor );
					$this->drawCenteredText($oppAngle - $this->acMcTextOffset, $this->acMcTextDistance-40, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

					// Record House Position for Plotting Aspects...
					$planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
					$this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
					$this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


				// Draw Standard House Divider
				} else {
					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->notchColor);
				}

				// Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
				$this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

				$houseCounter++;

				$accumulatedAngel -= 30;
			}

			// echo "<pre>";				
			// var_dump($this->houseAngles);
			// echo "</pre>";

			//var_dump($houseCollisions);
			foreach ($houseCollisions as $houseNumber => $collisionArray) {
				asort($collisionArray);

				$counter = 0;
				foreach ($collisionArray as $planet => $planetAngle) {

					if ( ($counter == 0) && (sizeof($collisionArray) == 1)) {
						// Check to see if it is bordering on any house boundary... and if it is lock it
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house... lock it.<br />';
						$this->_planetsLocked[] = $planet;
					} else if ($counter == 0) {
						$tmpHouseNum = ($houseNumber == 12 ? 1 : $houseNumber + 1);
						$diff = ( ($planetAngle - $this->houseAngles[$tmpHouseNum]) < 0 ? ($planetAngle - $this->houseAngles[$tmpHouseNum]) * -1 : ($planetAngle - $this->houseAngles[$tmpHouseNum]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the first one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the first one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					} else if ( ($counter == sizeof($collisionArray) - 1) ) {
						$diff = ( ($planetAngle - $this->houseAngles[$houseNumber]) < 0 ? ($planetAngle - $this->houseAngles[$houseNumber]) * -1 : ($planetAngle - $this->houseAngles[$houseNumber]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the last one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the last one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					}

					$counter++;

				}

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}

		public function drawActiveSides($filename = null, $branded = false) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

 			/**
			 * Display House Positions, MC and Ascendant
			 *
			 * TODO: Add thickness to antialiased lines..
			 */
			$houseCounter = 1;
			$accumulatedAngel = 0;
			$ascendantAngel = 0;
			
			foreach ($this->_houses as $house => $details) {				
				// echo "<pre>";				
				// var_dump($details);
				// echo "</pre>";
				
				// Find the angle of the house
				$houseRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
				$houseAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $houseRelativeAngle - 250 + $this->planetGlyphAngles[ "Sun" ]);				
				
				// echo "$house";
				// echo "<pre>";				
				// var_dump($houseRelativeAngle);
				// var_dump($houseAbsoluteAngle);
				// echo "</pre>";	

				// 30 Deg Houses
				if( $houseAbsoluteAngle >= 0) {
					$houseAbsoluteAngle = ( ( ( 30 - ( $houseAbsoluteAngle % 30 ) ) >= 15 ) ? ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % 30 ) ) : ( $houseAbsoluteAngle + ( 30 - ( $houseAbsoluteAngle % 30 ) ) ) );
				}
				else {
					$houseAbsoluteAngle = ( ( ( -30 - ( $houseAbsoluteAngle % ( -30 ) ) ) >= -15 ) ? ( $houseAbsoluteAngle + ( -30 ) - ( $houseAbsoluteAngle % ( -30 ) ) ) : ( $houseAbsoluteAngle - ( $houseAbsoluteAngle % ( -30 ) ) ) );
				}
				
				if ( $house == 'Ascendant' ) {
					$ascendantAngel = $houseAbsoluteAngle;
				}				
				else {
					$houseAbsoluteAngle = $ascendantAngel + $accumulatedAngel;
				}
				
				// echo "<pre>";		
				// var_dump($houseAbsoluteAngle);
				// echo "</pre><br>";				
				
				// Draw the Ascendant, Descendant, MC (MidHeaven) and IC
				if ( ($house == 'MC (Midheaven)') || ($house == 'Ascendant') ) {

					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
					$this->drawCenteredText($houseAbsoluteAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($house)], $this->acMcGlyphFontSize, $this->acMcColorActive );
					$this->drawCenteredText($houseAbsoluteAngle - $this->acMcTextOffset, $this->acMcTextDistance-40, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColorActive);					

					$oppAngle = $houseAbsoluteAngle - 180;
					if ($oppAngle <= 0) $oppAngle += 360;
					$oppGlyph = ($house == 'Ascendant' ? $this->signFont['descendant'] : $this->signFont['ic']);

					// Not sure why or how this works while it's commented out... but it does, otherwise we get a thick line for the DC.
					/*
					$this->drawPartialPolarLine($oppAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
					$this->drawPartialPolarLine($oppAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
					*/
					$this->drawCenteredText($oppAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $oppGlyph, $this->acMcGlyphFontSize, $this->acMcColorActive );
					$this->drawCenteredText($oppAngle - $this->acMcTextOffset, $this->acMcTextDistance-40, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColorActive);					

					// Record House Position for Plotting Aspects...
					$planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
					$this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
					$this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


				// Draw Standard House Divider
				} else {
					//$this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->notchColor);
				}

				// Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
				$this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

				$houseCounter++;

				$accumulatedAngel -= 30;
			}

			// echo "<pre>";				
			// var_dump($this->houseAngles);
			// echo "</pre>";

			//var_dump($houseCollisions);
			foreach ($houseCollisions as $houseNumber => $collisionArray) {
				asort($collisionArray);

				$counter = 0;
				foreach ($collisionArray as $planet => $planetAngle) {

					if ( ($counter == 0) && (sizeof($collisionArray) == 1)) {
						// Check to see if it is bordering on any house boundary... and if it is lock it
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house... lock it.<br />';
						$this->_planetsLocked[] = $planet;
					} else if ($counter == 0) {
						$tmpHouseNum = ($houseNumber == 12 ? 1 : $houseNumber + 1);
						$diff = ( ($planetAngle - $this->houseAngles[$tmpHouseNum]) < 0 ? ($planetAngle - $this->houseAngles[$tmpHouseNum]) * -1 : ($planetAngle - $this->houseAngles[$tmpHouseNum]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the first one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the first one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					} else if ( ($counter == sizeof($collisionArray) - 1) ) {
						$diff = ( ($planetAngle - $this->houseAngles[$houseNumber]) < 0 ? ($planetAngle - $this->houseAngles[$houseNumber]) * -1 : ($planetAngle - $this->houseAngles[$houseNumber]));
						//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the last one?<br />';

						// 5 is the angle difference, and then it is used later as well.
						if ($diff <= $this->planetCollisionLimit) {
							//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the last one?... lock it.<br />';
							$this->_planetsLocked[] = $planet;
						}

					}

					$counter++;

				}

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}

		public function drawRelations($filename = null, $relationNum = 1) {
			/**
			 * Setup some variables we will re-use throughout this script
			 */
			$this->chartWidth = 1200;
			$this->chartHeight = 1200;

			/**
			if (is_null($filename)) {
				$this->chartWidth = 600;
				$this->chartHeight = 600;
			}
			*/
			$chartOuterRadius = $this->chartWidth/2;
			$chartInnerRadius = ($this->chartWidth/2) - 100;

			/**
			 * Choose some common colours
			 */
			$chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
			$chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
			$notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


			/**
			 * Find Ascendant Offset
			 *
			 * This Determines at which angle we draw the chart
			 */
			$ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
			$ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

			/**
			 * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
			 */
			foreach ($this->signOffset as $sign => $offset) {
				$newOffset = $offset + $ascendantDiff;
				if ($newOffset > 360) $newOffset -= 360;
				if ($newOffset <= 0) $newOffset += 360;

				$this->signOffset[$sign] = $newOffset;
			}

			/**
			 * Create the Image as a PNG True Color and Fill it with White
			 */ 
			$this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
			imagesavealpha($this->image, true);
			imagealphablending($this->image, false);
			
			$whiteColor = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
			imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, $whiteColor);

			/**
			 * Draw the Planet Notches to Show Where they Occur
			 *
			 * NOTE: We also make a note of where they are being plotted so we can do draw the aspects later.
			 */
			foreach ($this->_planets as $planet => $details) {

				// Skip over planets we want to ignore
				if (in_array($planet, $this->ignorePlanets)) continue;

				// Find the angle of the sign
				$planetRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
				$planetAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $planetRelativeAngle);

				// Record the Planet Positions for Drawing Aspects... Record the Position at the Outer Radius of the Aspect Circle
				$this->planetPositions[$planet]['x'] = $this->getPolarX($planetAbsoluteAngle, $this->aspectOuterRadius);
				$this->planetPositions[$planet]['y'] = $this->getPolarY($planetAbsoluteAngle, $this->aspectOuterRadius);

				// Record the Angles so we can later do Collision Detection and Draw the Glyphs in the right spot
				$this->planetAngles[$planet] = $planetAbsoluteAngle;
				$this->planetGlyphAngles[$planet] = $planetAbsoluteAngle;
			}

			/**
			 * Draw in the Aspects
			 */
			$relationsCounter = 1;
			
			// echo "<pre>";
			// echo var_dump($this->_aspects);	
			// echo "</pre>";
			
			// echo "<pre>";
			// echo var_dump($this->aspectStyles);	
			// echo "</pre>";			
			
			foreach ($this->_aspects as $aspect) {
				
				if( $relationNum != 'all' ) {
					if( $relationsCounter != $relationNum ) {
						$relationsCounter++;
						
						continue;
					}
				}

				if ( (in_array($aspect['Planet1'], $this->ignoreAspects)) || (in_array($aspect['Planet2'], $this->ignoreAspects))) continue;

				if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'line') {

					AdGuru_Image_Toolbox::imageSmoothAlphaLine(
						$this->image,
						$this->planetPositions[$aspect['Planet1']]['x'],
						$this->planetPositions[$aspect['Planet1']]['y'],
						$this->planetPositions[$aspect['Planet2']]['x'],
						$this->planetPositions[$aspect['Planet2']]['y'],
						$this->aspectStyles[$aspect['Aspect']]['color']['R'], $this->aspectStyles[$aspect['Aspect']]['color']['G'], $this->aspectStyles[$aspect['Aspect']]['color']['B'], 0);

				} else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dashed') {

					$aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
					$aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
					$white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
					$style = array($aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white, $white);
					imagesetstyle($this->image, $style);
					imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

				} else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dotted') {

					$aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
					$aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
					$white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
					$style = array($aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white);
					imagesetstyle($this->image, $style);
					imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

				}
				
				$relationsCounter++;

			}

			//header( 'Content-Type: image/png' );
			if (is_null($filename)) {
				$filename = 'chart-' . mktime() . rand(1,20) . '.png';
				imagepng($this->image, $this->tempImagePath . $filename);
			} else {
				imagepng($this->image, $this->tempImagePath . $filename);
			}

			return $filename;
		}		

		public function initializeChart() {
			$this->chartWidth = 1200;
			$this->chartHeight = $this->chartWidth;

			$this->hamburgSymbolsPath = Config::FONTS_PATH . 'HamburgSymbols.ttf';
			$this->arialPath = Config::FONTS_PATH . 'arial.ttf';
			
			$this->tempImagePath = Config::TEMP_PATH;
			
			$this->acMcOuterRadius = $this->chartWidth/2;
			$this->acMcNotchOuterRadius = $this->acMcOuterRadius - 50;
			$this->starSignOuterRadius = $this->acMcNotchOuterRadius - ($this->notchSize2 * 2);
			$this->starSignNotchOuterRadius = $this->starSignOuterRadius - 100;
			$this->planetOuterRadius = $this->starSignNotchOuterRadius - ($this->notchSize2 * 2);
			$this->aspectOuterRadius = $this->planetOuterRadius - 150;
			$this->centerOuterRadius = 50;

			$this->acMcBackgroundColor = array( 'R' => 0xCD, 'G' => 0xE3, 'B' => 0xCA );
			$this->starSignBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0x87, 'A' => 127 );
			$this->planetBackgroundColor = array( 'R' => 0xCA, 'G' => 0xCE, 'B' => 0xE3, 'A' => 127 );
			$this->aspectBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF, 'A' => 127 );
			$this->centerBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF, 'A' => 127 );
			$this->notchBackgroundColor = array( 'R' => 0xFF, 'G' => 0x66, 'B' => 0x00, 'A' => 0 );
			$this->starSignNotchColor = array( 'R' => 0xFF, 'G' => 0x66, 'B' => 0x00 );
			$this->notchColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0x00 );
			$this->strokeColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );

			$this->starSignColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );
			$this->planetColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );
			$this->acMcColor = array( 'R' => 0xAC, 'G' => 0xE6, 'B' => 0x00 );
			$this->acMcColorActive = array( 'R' => 0xFF, 'G' => 0x00, 'B' => 0x00 );

			$this->ignorePlanets = array('Lilith', 'P. of Fortune', 'Vertex');
			$this->ignoreAspects = array('Ascendant', 'Chiron', 'Midheaven', 'Lilith', 'P. of Fortune', 'Vertex', 'True Node');

			$this->aspectStyles = array(
				'Square' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
				'Trine' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
				'Conjunction' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
				'Quincunx' => array('style' => 'dashed', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
				'Sextile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
				'Semisextile' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
				'Semisquare' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
				'Opposition' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
				'Sesquiquare' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
				'Quintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
				'Biquintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
			);

			$this->acMcGlyphDistance = $this->acMcOuterRadius - 20;
			$this->acMcTextDistance = $this->acMcOuterRadius - 25;

			$this->houseTextDistance = $this->centerOuterRadius + 100;

			$this->planetGlyphDistance = $this->planetOuterRadius - 35;
			$this->planetMinTextDistance = $this->planetGlyphDistance - 35;
			$this->planetSignGlyphDistance = $this->planetMinTextDistance - 25;
			$this->planetDegTextDistance = $this->planetSignGlyphDistance - 25;		
		}		
	}
?>