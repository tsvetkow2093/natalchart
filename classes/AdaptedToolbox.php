<?php

class AdaptedToolbox extends AdGuru_Image_Toolbox {
		
	public static function drawCenteredText($image, $x, $y, $font, $text, $size, $color = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00) ) {
print_r('xx');
    	$textBox = imagettfbbox($size,0,$font,$text);
        $textWidth = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);

        imagettftext(
            $image,
            $size,
            0,
            $x - ($textWidth/2),
            $y + ($textHeight/2),
            imagecolorallocate($image, $color['R'], $color['G'], $color['B']),
            $font,
            $text);

    }	
	
	public static function imageSmoothCircle( &$img, $cx, $cy, $cr, $color ) {
	    $ir = $cr;
	    $ix = 0;
	    $iy = $ir;
	    $ig = 2 * $ir - 3;
	    $idgr = -6;
	    $idgd = 4 * $ir - 10;
	    $fill = imageColorExactAlpha( $img, $color[ 'R' ], $color[ 'G' ], $color[ 'B' ], $color[ 'A' ] );
	    imageLine( $img, $cx + $cr - 1, $cy, $cx, $cy, $fill );
	    imageLine( $img, $cx - $cr + 1, $cy, $cx - 1, $cy, $fill );
	    imageLine( $img, $cx, $cy + $cr - 1, $cx, $cy + 1, $fill );
	    imageLine( $img, $cx, $cy - $cr + 1, $cx, $cy - 1, $fill );
	    $draw = imageColorExactAlpha( $img, $color[ 'R' ], $color[ 'G' ], $color[ 'B' ], $color[ 'A' ] );
	    imageSetPixel( $img, $cx + $cr, $cy, $draw );
	    imageSetPixel( $img, $cx - $cr, $cy, $draw );
	    imageSetPixel( $img, $cx, $cy + $cr, $draw );
	    imageSetPixel( $img, $cx, $cy - $cr, $draw );
	    while ( $ix <= $iy - 2 ) {
	        if ( $ig < 0 ) {
	            $ig += $idgd;
	            $idgd -= 8;
	            $iy--;
	        } else {
	            $ig += $idgr;
	            $idgd -= 4;
	        }
	        $idgr -= 4;
	        $ix++;
	        imageLine( $img, $cx + $ix, $cy + $iy - 1, $cx + $ix, $cy + $ix, $fill );
	        imageLine( $img, $cx + $ix, $cy - $iy + 1, $cx + $ix, $cy - $ix, $fill );
	        imageLine( $img, $cx - $ix, $cy + $iy - 1, $cx - $ix, $cy + $ix, $fill );
	        imageLine( $img, $cx - $ix, $cy - $iy + 1, $cx - $ix, $cy - $ix, $fill );
	        imageLine( $img, $cx + $iy - 1, $cy + $ix, $cx + $ix, $cy + $ix, $fill );
	        imageLine( $img, $cx + $iy - 1, $cy - $ix, $cx + $ix, $cy - $ix, $fill );
	        imageLine( $img, $cx - $iy + 1, $cy + $ix, $cx - $ix, $cy + $ix, $fill );
	        imageLine( $img, $cx - $iy + 1, $cy - $ix, $cx - $ix, $cy - $ix, $fill );
	        $filled = 0;
	        for ( $xx = $ix - 0.45; $xx < $ix + 0.5; $xx += 0.2 ) {
	            for ( $yy = $iy - 0.45; $yy < $iy + 0.5; $yy += 0.2 ) {
	                if ( sqrt( pow( $xx, 2 ) + pow( $yy, 2 ) ) < $cr ) $filled += 4;
	            }
	        }
	        $draw = imageColorExactAlpha( $img, $color[ 'R' ], $color[ 'G' ], $color[ 'B' ], ( 100 - $filled ) );
	        imageSetPixel( $img, $cx + $ix, $cy + $iy, $draw );
	        imageSetPixel( $img, $cx + $ix, $cy - $iy, $draw );
	        imageSetPixel( $img, $cx - $ix, $cy + $iy, $draw );
	        imageSetPixel( $img, $cx - $ix, $cy - $iy, $draw );
	        imageSetPixel( $img, $cx + $iy, $cy + $ix, $draw );
	        imageSetPixel( $img, $cx + $iy, $cy - $ix, $draw );
	        imageSetPixel( $img, $cx - $iy, $cy + $ix, $draw );
	        imageSetPixel( $img, $cx - $iy, $cy - $ix, $draw );
	    }
	}
	
	
	
	
	public static function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
	{
	    /* this way it works well only for orthogonal lines
	    imagesetthickness($image, $thick);
	    return imageline($image, $x1, $y1, $x2, $y2, $color);
	    */
	    if ($thick == 1) {
	        return imageline($image, $x1, $y1, $x2, $y2, $color);
	    }
	    $t = $thick / 2 - 0.5;
	    if ($x1 == $x2 || $y1 == $y2) {
	        return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
	    }
	    $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
	    $a = $t / sqrt(1 + pow($k, 2));
	    $points = array(
	        round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
	        round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
	        round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
	        round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
	    );
	    imagefilledpolygon($image, $points, 4, $color);
	    return imagepolygon($image, $points, 4, $color);
	}
	
	public static function imagelinedotted ($im, $x1, $y1, $x2, $y2, $dist, $col) {
	    $transp = imagecolortransparent ($im);
	
	    $style = array ($col);
	
	    for ($i=0; $i<$dist; $i++) {
	        array_push($style, $transp);        // Generate style array - loop needed for customisable distance between the dots
	    }
	
	    imagesetstyle ($im, $style);
	    return (integer) imageline ($im, $x1, $y1, $x2, $y2, IMG_COLOR_STYLED);
	    imagesetstyle ($im, array($col));        // Reset style - just in case...
	}
	
	/**
	* I've modified the previous entry for drawing on a polar coordinate system to better represent angles based on a 360� whole circle bearing.
	*/
	public static function imagepolarline($image,$x1,$y1,$length,$angle,$color)
	{
	
	    $x2 = $x1 + sin( deg2rad($angle) ) * $length;
	    $y2 = $y1 + cos( deg2rad($angle+180) ) * $length;
	
	    //imageline($image,$x1,$y1,$x2,$y2,$color);
	    imageSmoothAlphaLine($image, $x1, $y1, $x2, $y2, $color['R'], $color['G'], $color['B'], 0);
	}
	
	
	
	
	
	/**
	 * public static function imageSmoothAlphaLine() - version 1.0
	 * Draws a smooth line with alpha-public static functionality
	 *
	 * @param   ident    the image to draw on
	 * @param   integer  x1
	 * @param   integer  y1
	 * @param   integer  x2
	 * @param   integer  y2
	 * @param   integer  red (0 to 255)
	 * @param   integer  green (0 to 255)
	 * @param   integer  blue (0 to 255)
	 * @param   integer  alpha (0 to 127)
	 *
	 * @access  public
	 *
	 * @author  DASPRiD <d@sprid.de>
	 */
	public static function imageSmoothAlphaLine ($image, $x1, $y1, $x2, $y2, $r, $g, $b, $alpha=0) {
	  $icr = $r;
	  $icg = $g;
	  $icb = $b;
	  $dcol = imagecolorallocatealpha($image, $icr, $icg, $icb, $alpha);
	
	  if ($y1 == $y2 || $x1 == $x2)
	    imageline($image, $x1, $y2, $x1, $y2, $dcol);
	  else {
	    $m = ($y2 - $y1) / ($x2 - $x1);
	    $b = $y1 - $m * $x1;
	
	    if (abs ($m) <2) {
	      $x = min($x1, $x2);
	      $endx = max($x1, $x2) + 1;
	
	      while ($x < $endx) {
	        $y = $m * $x + $b;
	        $ya = ($y == floor($y) ? 1: $y - floor($y));
	        $yb = ceil($y) - $y;
	
	        $trgb = ImageColorAt($image, $x, floor($y));
	        $tcr = ($trgb >> 16) & 0xFF;
	        $tcg = ($trgb >> 8) & 0xFF;
	        $tcb = $trgb & 0xFF;
	        imagesetpixel($image, $x, floor($y), imagecolorallocatealpha($image, ($tcr * $ya + $icr * $yb), ($tcg * $ya + $icg * $yb), ($tcb * $ya + $icb * $yb), $alpha));
	
	        $trgb = ImageColorAt($image, $x, ceil($y));
	        $tcr = ($trgb >> 16) & 0xFF;
	        $tcg = ($trgb >> 8) & 0xFF;
	        $tcb = $trgb & 0xFF;
	        imagesetpixel($image, $x, ceil($y), imagecolorallocatealpha($image, ($tcr * $yb + $icr * $ya), ($tcg * $yb + $icg * $ya), ($tcb * $yb + $icb * $ya), $alpha));
	
	        $x++;
	      }
	    } else {
	      $y = min($y1, $y2);
	      $endy = max($y1, $y2) + 1;
	
	      while ($y < $endy) {
	        $x = ($y - $b) / $m;
	        $xa = ($x == floor($x) ? 1: $x - floor($x));
	        $xb = ceil($x) - $x;
	
	        $trgb = ImageColorAt($image, floor($x), $y);
	        $tcr = ($trgb >> 16) & 0xFF;
	        $tcg = ($trgb >> 8) & 0xFF;
	        $tcb = $trgb & 0xFF;
	        imagesetpixel($image, floor($x), $y, imagecolorallocatealpha($image, ($tcr * $xa + $icr * $xb), ($tcg * $xa + $icg * $xb), ($tcb * $xa + $icb * $xb), $alpha));
	
	        $trgb = ImageColorAt($image, ceil($x), $y);
	        $tcr = ($trgb >> 16) & 0xFF;
	        $tcg = ($trgb >> 8) & 0xFF;
	        $tcb = $trgb & 0xFF;
	        imagesetpixel ($image, ceil($x), $y, imagecolorallocatealpha($image, ($tcr * $xb + $icr * $xa), ($tcg * $xb + $icg * $xa), ($tcb * $xb + $icb * $xa), $alpha));
	
	        $y ++;
	      }
	    }
	  }
	} // end of 'imageSmoothAlphaLine()' public static function
	
	public static function convertDecToDeg( $angel ) {
		$degrees = floor( $angel );

		$minutes = floor( ( ( $angel - $degrees ) * 60 ) );

		$seconds = floor( ( ( $angel - $degrees - $minutes / 60) * 3600 ) );

		return ( $degrees . '&deg; ' . $minutes . "&prime; " . $seconds . "&Prime;" );
	}
	
	public static function convertDegToDec($degrees, $minutes, $seconds) {
	    return floatval($degrees) + ((floatval($minutes) + (floatval($seconds)/60)) / 60);
	}
	
	public static function convertDegArrayToDec($properties) {
	    return floatval($properties['deg']) + ((floatval($properties['min']) + (floatval($properties['sec'])/60)) / 60);
	}	
	
}
?>