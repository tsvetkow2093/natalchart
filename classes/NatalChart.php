<?php

class Calculation_Astrology_NatalChart extends Calculation_Astrology_Abstract {
	
	const SE_SUN = 0;
	const SE_MOON = 1;
	const SE_MERCURY = 2;
	const SE_VENUS = 3;
	const SE_MARS = 4;
	const SE_JUPITER = 5;
	const SE_SATURN = 6;
	const SE_URANUS = 7;
	const SE_NEPTUNE = 8;
	const SE_PLUTO = 9;
	const SE_CHIRON = 10;
	const SE_LILITH = 11;
	const SE_TNODE = 12;
	const SE_POF = 13;
	const SE_VERTEX = 14;
	const LAST_PLANET = 14;
	
	public $personalData;
	
	public $image;
    public $chartWidth = 0;
    public $chartHeight = 0;
	public $tempImagePath;
	public $swephPath;
	
    protected $acMcOuterRadius;
    protected $acMcNotchOuterRadius;
    protected $starSignOuterRadius;
    protected $starSignNotchOuterRadius;
    protected $planetOuterRadius;
    protected $aspectOuterRadius;
    protected $centerOuterRadius;

    protected $acMcBackgroundColor;
    protected $starSignBackgroundColor;
    protected $starSignNotchColor;
    protected $planetBackgroundColor;
    protected $aspectBackgroundColor;
    protected $centerBackgroundColor;
    protected $notchBackgroundColor;
    protected $notchColor;
    protected $strokeColor;

    protected $starSignColor;
    protected $aspectStyles;

    protected $circleStroke = 15;
    protected $notchSize1 = 5;
    protected $notchSize2 = 10;
    protected $starSignNotchSize = 35;
    protected $acMcNotchSize = 35;
    protected $starSignSize = 40;
    protected $acMcGlyphFontSize = 20;
    protected $acMcTextFontSize = 12;
    protected $houseTextFontSize = 12;
    protected $planetGlyphFontSize = 25;
    protected $planetSignGlyphFontSize = 12;
    protected $planetTextFontSize = 11;

    protected $hamburgSymbolsPath;
    protected $arialPath;

    protected $acMcGlyphDistance;
    protected $acMcTextDistance;
    protected $acMcColor;

    public $ignorePlanets;
    public $ignoreAspects;

    protected $planetPositions;
    protected $houseAngles = array();
    protected $planetAngles = array();
    protected $planetGlyphAngles = array();
    protected $planetsLocked = array();

    protected $houseTextDistance;

    protected $planetGlyphDistance;
    protected $planetDegTextDistance;
    protected $planetSignGlyphDistance;
    protected $planetMinTextDistance;

    protected $planetColor;

    /**
     * Measured in Degrees
     */
    protected $starSignSlice = 30;
    protected $acMcTextOffset = 3.2;
    protected $planetCollisionLimit = 5;

    protected $ascendantConstantAngle = 270;	
	protected $_data = array(
			'current_first_name' => null,
			'current_last_name' => null,
			'birth_full_name' => null,
			'acs_location_id' => null,
			'birth_date' => null,
			'birth_time' => null
		);
		
	protected $_birthDate;
	protected $_location;
	protected $_timezone;
	
	public $_houses;
	public $_planets;
	public $_aspects;
	
	public function __construct($data = null) {
		if (!is_null($data))
			$this->setFromArray($data);
			
        $this->swephPath = Zend_Registry::get('configuration')->sweph->path;
			
		$this->_birthDate = $this->getBirthDate();
		$this->_location = $this->getLocationRow();
		$this->_timezone = $this->getTimezone();
		$this->calculate();
		$this->initializeChart();
	}
	
	public function calculate() {
		
	    $month      = $this->getBirthDate('M');
	    $day        = $this->getBirthDate('d');
	    $year       = $this->getBirthDate('Y');
	
	    $hour       = $this->getBirthDate('H');
	    $minute     = $this->getBirthDate('m');
	
	    // Need to add -1 or +1 depending on DST.
	    $timezone   = $this->getTimezone()->getBase() / 900;
	    $timezone   = ( ($this->getTimezone()->getType() == 1) && ($this->getTimezone()->getBase() < 0) ? $timezone - 1 : $timezone );
	
	    // Get Longitude Values
	    $longitudeArray     = array();
	    preg_match('/([0-9]*)(E|W)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->longitude, $longitudeArray);
	
	    $long_deg   = intval($longitudeArray[1]);
	    $long_min   = intval($longitudeArray[3]);
	    $ew         = ($longitudeArray[2] == 'E' ? 1 : -1 );
	
	    // Get Latitude Values
	    $latitudeArray      = array();
	    preg_match('/([0-9]*)(N|S)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->latitude, $latitudeArray);
	
	    $lat_deg   = intval($latitudeArray[1]);
	    $lat_min   = intval($latitudeArray[3]);
	    $ns         = ($latitudeArray[2] == 'N' ? 1 : -1 );
	
	    // TODO: Move: calculate astronomic data
	    $swephsrc   = 'sweph';
	    $sweph      = 'sweph';
		
	    //assign data from database to local variables
	    $inmonth    = $month;
	    $inday      = $day;
	    $inyear     = $year;
	
	    $inhours    = $hour;
	    $inmins     = $minute;
	    $insecs     = "0";
	
	    $intz       = $timezone * -1;
	
	    $my_longitude   = $ew * ($long_deg + ($long_min / 60));
	    $my_latitude    = $ns * ($lat_deg + ($lat_min / 60));
	
	    if ($intz >= 0) {
	        $whole      = floor($intz);
	        $fraction   = $intz - floor($intz);
	    } else {
	        $whole      = ceil($intz);
	        $fraction   = $intz - ceil($intz);
	    }
	
	    $inhours    = $inhours - $whole;
	    $inmins     = $inmins - ($fraction * 60);
	    
	    // adjust date and time for minus hour due to time zone taking the hour negative
	    $utdatenow  = strftime("%d.%m.%Y", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));
	    $utnow      = strftime("%H:%M:%S", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));
	
	    // get LAST_PLANET planets and all house cusps
	    $h_sys = "p";
	
	    if (APPLICATION_ENV != 'production') {
	    	$out = file(Zend_Registry::get('configuration')->sweph->sample->out);
	    } else {
	    	exec ($this->swephPath . "swetest -edir" . $this->swephPath . " -b$utdatenow -ut$utnow -p0123456789DAttt -eswe -house$my_longitude,$my_latitude,$h_sys -flsj -g, -head", $out);
	    }	
	    
	    // Each line of output data from swetest is exploded into array $row, giving these elements:
	    // 0 = longitude
	    // 1 = speed
	    // 2 = house position
	    // planets are index 0 - index (LAST_PLANET), house cusps are index (LAST_PLANET + 1) - (LAST_PLANET + 12)
	    $longitude1 = array();
	    $speed1 = array();
	    $house_pos1 = array();
	    foreach ($out as $key => $line) {
	        $row = explode(',',$line);
	        
	        $longitude1[$key] = floatval($row[0]);
	        $speed1[$key] = floatval($row[1]);
	        $house_pos1[$key] = (isset($row[2]) ? floatval($row[2]) : 0);
	    }
	
	      //include("constants.php");			// this is here because we must rename the planet names
	
		$pl_name = array();	      
		$pl_name[0] = "Sun";
		$pl_name[1] = "Moon";
		$pl_name[2] = "Mercury";
		$pl_name[3] = "Venus";
		$pl_name[4] = "Mars";
		$pl_name[5] = "Jupiter";
		$pl_name[6] = "Saturn";
		$pl_name[7] = "Uranus";
		$pl_name[8] = "Neptune";
		$pl_name[9] = "Pluto";
		$pl_name[10] = "Chiron";
		$pl_name[11] = "Lilith";
		$pl_name[12] = "True Node";
		$pl_name[13] = "P. of Fortune";		//add a planet
		$pl_name[14] = "Vertex";
		$pl_name[self::LAST_PLANET + 1] = "Ascendant";
		$pl_name[self::LAST_PLANET + 2] = "Midheaven";
		
		$pl_name[self::LAST_PLANET + 1] = "Ascendant";
		$pl_name[self::LAST_PLANET + 2] = "House 2";
		$pl_name[self::LAST_PLANET + 3] = "House 3";
		$pl_name[self::LAST_PLANET + 4] = "House 4";
		$pl_name[self::LAST_PLANET + 5] = "House 5";
		$pl_name[self::LAST_PLANET + 6] = "House 6";
		$pl_name[self::LAST_PLANET + 7] = "House 7";
		$pl_name[self::LAST_PLANET + 8] = "House 8";
		$pl_name[self::LAST_PLANET + 9] = "House 9";
		$pl_name[self::LAST_PLANET + 10] = "MC (Midheaven)";
		$pl_name[self::LAST_PLANET + 11] = "House 11";
		$pl_name[self::LAST_PLANET + 12] = "House 12";
	      
	    //calculate the Part of Fortune
	    //is this a day chart or a night chart?
	    if ($longitude1[self::LAST_PLANET + 1] > $longitude1[self::LAST_PLANET + 7]) {
	
	        if ($longitude1[0] <= $longitude1[self::LAST_PLANET + 1] && $longitude1[0] > $longitude1[self::LAST_PLANET + 7]) {
	            $day_chart = true;
	        } else {
	            $day_chart = false;
	        }
	
	    } else {
	
	        if ($longitude1[0] > $longitude1[self::LAST_PLANET + 1] && $longitude1[0] <= $longitude1[self::LAST_PLANET + 7]) {
	            $day_chart = false;
	        } else {
	            $day_chart = true;
	        }
	
	    }
	
	    if ($day_chart == true) {
	        $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] + $longitude1[1] - $longitude1[0];
	    } else {
	        $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] - $longitude1[1] + $longitude1[0];
	    }
	
	    if ($longitude1[self::SE_POF] >= 360) {
	        $longitude1[self::SE_POF] = $longitude1[self::SE_POF] - 360;
	    }
	
	    if ($longitude1[self::SE_POF] < 0) {
	        $longitude1[self::SE_POF] = $longitude1[self::SE_POF] + 360;
	    }
	
	    //add a planet - maybe some code needs to be put here
	
	    //capture the Vertex longitude
	    $longitude1[self::LAST_PLANET] = $longitude1[self::LAST_PLANET + 16];		//Asc = +13, MC = +14, RAMC = +15, Vertex = +16
	
	
	    //get house positions of planets here
	    for ($x = 1; $x <= 12; $x++) {
	
	        for ($y = 0; $y <= self::LAST_PLANET; $y++) {
	
	            $pl = $longitude1[$y] + (1 / 36000);
	
	            if ($x < 12 && $longitude1[$x + self::LAST_PLANET] > $longitude1[$x + self::LAST_PLANET + 1]) {
	
	                if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[$x + self::LAST_PLANET + 1] && $pl >= 0)) {
	
	                    $house_pos1[$y] = $x;
	                    continue;
	
	                }
	
	            }
	
	            if ($x == 12 && ($longitude1[$x + self::LAST_PLANET] > $longitude1[self::LAST_PLANET + 1])) {
	
	                if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[self::LAST_PLANET + 1] && $pl >= 0)) {
	                    $house_pos1[$y] = $x;
	                }
	
	                continue;
	            }
	
	            if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[$x + self::LAST_PLANET + 1]) && ($x < 12)) {
	
	                $house_pos1[$y] = $x;
	                continue;
	            }
	
	            if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[self::LAST_PLANET + 1]) && ($x == 12)) {
	                $house_pos1[$y] = $x;
	            }
	        }
	    }
	
	    //display natal data
	    $secs = "0";
	    if ($timezone < 0) {
	        $tz = $timezone;
	    } else {
	        $tz = "+" . $timezone;
	    }
	
	    $hr_ob = $hour;
	    $min_ob = $minute;
	
	    $ubt1 = 0;
	    if (($hr_ob == 12) && ($min_ob == 0)) {
	        $ubt1 = 1;				// this person has an unknown birth time
	    }
	
	    $ubt2 = $ubt1;
	
	    $rx1 = "";
	    for ($i = 0; $i <= self::SE_TNODE; $i++) {
	
	        if ($speed1[$i] < 0) {
	            $rx1 .= "R";
	        } else {
	            $rx1 .= " ";
	        }
	    }
	
	    $rx2 = $rx1;
	
	    for ($i = 1; $i <= self::LAST_PLANET; $i++) {
	        $hc1[$i] = $longitude1[self::LAST_PLANET + $i];
	    }
	
	    if ($ubt1 == 1) {
	        $a1 = self::SE_TNODE;
	    } else {
	        $a1 = self::LAST_PLANET;
	    }
	
	    $planets = array();
	
	    for ($i = 0; $i <= $a1; $i++) {
	
	        if ($ubt1 == 1) {
	            $hse = "&nbsp;";
	        } else {
	            $hse = floor($house_pos1[$i]);
	        }
	
	        $planets[$pl_name[$i]] = array_merge($this->convertLongitudeToArray($longitude1[$i]), array('mid' => $this->mid($rx1, $i + 1, 1), 'house' => $hse));
	    }
	
	    if ($ubt1 == 0) {
	
	        $houses = array();
	
	        for ($i = self::LAST_PLANET + 1; $i <= self::LAST_PLANET + 12; $i++) {
	
	            if ($i == self::LAST_PLANET + 1) {
	                $name = 'Ascendant';
	            } elseif ($i == self::LAST_PLANET + 10) {
	                $name = 'MC (Midheaven)';
	            } else {
	                $name = 'House ' . ($i - self::LAST_PLANET);
	            }
	
	            $houses[$name] = $this->convertLongitudeToArray($longitude1[$i]);
	        }
	    }
	
	    $aspects = array();
	
	    // include Ascendant and MC
	    $longitude1[self::LAST_PLANET + 1] = $hc1[1];
	    $longitude1[self::LAST_PLANET + 2] = $hc1[10];
	
	    $pl_name[self::LAST_PLANET + 1] = "Ascendant";
	    $pl_name[self::LAST_PLANET + 2] = "Midheaven";
	
	    if ($ubt1 == 1) {
	        $a1 = self::SE_TNODE;
	    } else {
	        $a1 = self::LAST_PLANET + 2;
	    }
	
	    /**
	     * Setup Aspect Testing array
	     */
	    $aspectOrbs = array(
	        'conjunction' => array(
	                'sun' => 10,
	                'moon' => 10,
	                'mercury' => 10,
	                'venus' => 10,
	                'mars' => 10,
	                'jupiter' => 10,
	                'saturn' => 10,
	                'neptune' => 10,
	                'uranus' => 10,
	                'pluto' => 10,
	                'chiron' => 10,
	                'true node' => 10
	            ),
	        'opposition' => array(
	                'sun' => 10,
	                'moon' => 10,
	                'mercury' => 10,
	                'venus' => 10,
	                'mars' => 10,
	                'jupiter' => 10,
	                'saturn' => 10,
	                'neptune' => 10,
	                'uranus' => 10,
	                'pluto' => 10,
	                'chiron' => 10,
	                'true node' => 10
	            ),
	        'square' => array(
	                'sun' => 10,
	                'moon' => 10,
	                'mercury' => 10,
	                'venus' => 10,
	                'mars' => 10,
	                'jupiter' => 10,
	                'saturn' => 10,
	                'neptune' => 10,
	                'uranus' => 10,
	                'pluto' => 10,
	                'chiron' => 10,
	                'true node' => 10
	            ),
	        'trine' => array(
	                'sun' => 10,
	                'moon' => 10,
	                'mercury' => 10,
	                'venus' => 10,
	                'mars' => 10,
	                'jupiter' => 10,
	                'saturn' => 10,
	                'neptune' => 10,
	                'uranus' => 10,
	                'pluto' => 10,
	                'chiron' => 10,
	                'true node' => 10
	            ),
	        'sextile' => array(
	                'sun' => 6,
	                'moon' => 6,
	                'mercury' => 6,
	                'venus' => 6,
	                'mars' => 6,
	                'jupiter' => 6,
	                'saturn' => 6,
	                'neptune' => 6,
	                'uranus' => 6,
	                'pluto' => 6,
	                'chiron' => 6,
	                'true node' => 6
	            ),
	        'semisextile' => array(
	                'sun' => 3,
	                'moon' => 3,
	                'mercury' => 3,
	                'venus' => 3,
	                'mars' => 3,
	                'jupiter' => 3,
	                'saturn' => 3,
	                'neptune' => 3,
	                'uranus' => 3,
	                'pluto' => 3,
	                'chiron' => 3,
	                'true node' => 3
	            ),
	        'quincunx' => array(
	                'sun' => 3,
	                'moon' => 3,
	                'mercury' => 3,
	                'venus' => 3,
	                'mars' => 3,
	                'jupiter' => 3,
	                'saturn' => 3,
	                'neptune' => 3,
	                'uranus' => 3,
	                'pluto' => 3,
	                'chiron' => 3,
	                'true node' => 3
	            ),
	        'semisquare' => array(
	                'sun' => 3,
	                'moon' => 3,
	                'mercury' => 3,
	                'venus' => 3,
	                'mars' => 3,
	                'jupiter' => 3,
	                'saturn' => 3,
	                'neptune' => 3,
	                'uranus' => 3,
	                'pluto' => 3,
	                'chiron' => 3,
	                'true node' => 3
	            ),
	        'sesquiquare' => array(
	                'sun' => 3,
	                'moon' => 3,
	                'mercury' => 3,
	                'venus' => 3,
	                'mars' => 3,
	                'jupiter' => 3,
	                'saturn' => 3,
	                'neptune' => 3,
	                'uranus' => 3,
	                'pluto' => 3,
	                'chiron' => 3,
	                'true node' => 3
	            ),
	        'quintile' => array(
	                'sun' => 1,
	                'moon' => 1,
	                'mercury' => 1,
	                'venus' => 1,
	                'mars' => 1,
	                'jupiter' => 1,
	                'saturn' => 1,
	                'neptune' => 1,
	                'uranus' => 1,
	                'pluto' => 1,
	                'chiron' => 1,
	                'true node' => 1
	            ),
	        'biquintile' => array(
	                'sun' => 1,
	                'moon' => 1,
	                'mercury' => 1,
	                'venus' => 1,
	                'mars' => 1,
	                'jupiter' => 1,
	                'saturn' => 1,
	                'neptune' => 1,
	                'uranus' => 1,
	                'pluto' => 1,
	                'chiron' => 1,
	                'true node' => 1
	            ),
	
	
	    );
	
	    $aspectAngles = array(
	        'conjunction' => 0,
	        'opposition' => 180,
	        'square' => 90,
	        'trine' => 120,
	        'sextile' => 60,
	        'semisextile' => 30,
	        'quincunx' => 150,
	        'semisquare' => 45,
	        'sesquiquare' => 135,
	        'quintile' => 72,
	        'biquintile' => 144,
	    );
	
		$asp_name[1] = "conjunction";
		$asp_name[2] = "opposition";
		$asp_name[3] = "trine";
		$asp_name[4] = "square";
		$asp_name[5] = "quincunx";
		$asp_name[6] = "sextile";
		/**
		 * Aspects added by Ant
		 */
		$asp_name[7] = 'semisquare';
		$asp_name[8] = 'semisextile';
		$asp_name[9] = 'sesquiquare';
		$asp_name[10] = 'quintile';
		$asp_name[11] = 'biquintile';
	    
	    $aspectNamesFlipped = array_flip($asp_name);
	
	    /**
	     * Calculate Aspects
	     */
	    for ($i = 0; $i <= $a1; $i++) {
	
	        for ($j = 0; $j <= $a1; $j++) {
	
	            $q = 0;
	            $da = abs($longitude1[$i] - $longitude1[$j]);
	
	            if ($da > 180) {
	                $da = 360 - $da;
	            }
	
	            /**
	             * Using: http://www.astro.com/astrology/in_aspect_e.htm as guideline
	             */
	
	            foreach ($aspectAngles as $aspect => $absoluteAngle) {
	
	            	// TODO: Throws warnings on $pl_name['vertex'] etc.
	            	if (!isset($aspectOrbs[$aspect][strtolower($pl_name[$i])])) continue;
	                if ( ($da <=  ($absoluteAngle + $aspectOrbs[$aspect][strtolower($pl_name[$i])])) && ($da >=  ($absoluteAngle - $aspectOrbs[$aspect][strtolower($pl_name[$i])])) ) {
	                    $q = $aspectNamesFlipped[$aspect];
	                    $dax = $da;
	                }
	
	            }
	
	            /**
	             * Add aspects to the aspects array
	             *
	             * Record Planet 1, Aspect, Planet 2 and the Orb.
	             */
	            if ($q > 0 And $i != $j) {
	                $aspects[] = array('Planet1' => $pl_name[$i], 'Aspect' => ucwords($asp_name[$q]), 'Planet2' => $pl_name[$j], 'Orb' => sprintf("%.2f", abs($dax)));
	            }
	        }
	    }

	    $this->_planets = $planets;
	    $this->_houses = $houses;
	    $this->_aspects = $aspects;
		
	}
	
	public function drawChart($filename = null, $branded = false) {
        /**
         * Setup some variables we will re-use throughout this script
         */
        $this->chartWidth = 1200;
        $this->chartHeight = 1200;

        /**
        if (is_null($filename)) {
            $this->chartWidth = 600;
            $this->chartHeight = 600;
        }
        */
        $chartOuterRadius = $this->chartWidth/2;
        $chartInnerRadius = ($this->chartWidth/2) - 100;

        /**
         * Choose some common colours
         */
        $chartOuterColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
        $chartInnerColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
        $notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );


        /**
         * Find Ascendant Offset
         *
         * This Determines at which angle we draw the chart
         */
        $ascendantAbsoluteAngle = $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant']));
        $ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

        /**
         * Adjust our Sign Offsets by the Ascendant Offset so it appears as if the Ascendant is running from left to right along the chart at 270 degrees.
         */
        foreach ($this->signOffset as $sign => $offset) {
			var_dump($offset);
			
            $newOffset = $offset + $ascendantDiff;
            if ($newOffset > 360) $newOffset -= 360;
            if ($newOffset <= 0) $newOffset += 360;

        	$this->signOffset[$sign] = $newOffset;
        }

        /**
         * Create the Image as a PNG True Color and Fill it with White
         */
        $this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
        imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, imagecolorallocate($this->image, 255, 255, 255));
        
		/**
         * Brand it if necessary
         */
		if($branded) {

	        $textBox = imagettfbbox(24,45,$this->arialPath, $this->getFirstName());
	        $textWidth = abs($textBox[4] - $textBox[0]);
	        $textHeight = abs($textBox[5] - $textBox[1]);
			
			imagettftext(
				$this->image,
	            24,
	            45,
	            $this->getPolarX(315, 685) - ($textWidth/2),
	            $this->getPolarY(315, 685) + ($textHeight/2),
            	imagecolorallocate($this->image, 0, 0, 0),
	            $this->arialPath,
	            $this->getFirstName());
			
	            //date('g:i a \o\n l \t\h\e jS \of F Y', mktime($hour, $minute, 0, $month, $day, $year))

	        $textBox = imagettfbbox(18,45,$this->arialPath, $this->getBirthDate('h:m a MMMM j, Y'));
	        $textWidth = abs($textBox[4] - $textBox[0]);
	        $textHeight = abs($textBox[5] - $textBox[1]);
			
			imagettftext(
				$this->image,
	            18,
	            45,
	            $this->getPolarX(315, 645) - ($textWidth/2),
	            $this->getPolarY(315, 645) + ($textHeight/2),
            	imagecolorallocate($this->image, 0, 0, 0),
	            $this->arialPath,
	            $this->getBirthDate('h:m a MMMM j, Y'));
	            
	            
	            
		}

        /**
         * Draw the Circles and Background Colors which everything will sit on top of
         */
        $this->drawCenteredCircle($this->acMcNotchOuterRadius, $this->notchBackgroundColor);
        $this->drawCenteredCircle($this->starSignOuterRadius, $this->starSignBackgroundColor);
        $this->drawCenteredCircle($this->starSignNotchOuterRadius, $this->notchBackgroundColor);
        $this->drawCenteredCircle($this->planetOuterRadius, $this->planetBackgroundColor);
        $this->drawCenteredCircle($this->aspectOuterRadius, $this->aspectBackgroundColor);
        $this->drawCenteredCircle($this->centerOuterRadius, $this->centerBackgroundColor);

        /**
         * Draw the Notches in the Notch Areas
         *
         * Small notches 1 degree apart, Second Notches 5 degrees apart
         */
        for ($i = 0; $i <= 360; $i += 1) {
            $newOffset = $i + $ascendantDiff;
            if ($newOffset > 360) $newOffset -= 360;
            if ($newOffset <= 0) $newOffset += 360;

            if ( ($i % 30) == 0 ) continue;

            // When drawing the notches on the outer circle we need to take into account the circles outer stroke...
            if ( ($i % 5) == 0 ) {

                $this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize2, $this->notchSize2, $this->notchColor);
                $this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize2, $this->notchColor);

            } else {

                $this->drawPartialPolarLine($newOffset, $this->starSignNotchOuterRadius - $this->notchSize1, $this->notchSize1, $this->notchColor);
                $this->drawPartialPolarLine($newOffset, $this->starSignOuterRadius + $this->circleStroke, $this->notchSize1, $this->notchColor);

            }

        }

        /**
         * Draw Star Sign Dividers
         */
        foreach ($this->signOffset as $i) {
            $this->drawPartialPolarLine($i, $this->planetOuterRadius, $this->acMcNotchOuterRadius - $this->planetOuterRadius, $this->notchColor);
        }

        /**
         * Draw Star Sign Glyphs
         */

        foreach ($this->signOffset as $sign => $offset) {
            $this->drawCenteredText(
                $offset + $this->starSignSlice/2,
                $this->starSignOuterRadius - ($this->starSignOuterRadius - $this->starSignNotchOuterRadius) / 2,
                $this->hamburgSymbolsPath,
                $this->signFont[$sign],
                $this->starSignSize,
                $this->starSignColor
            );

        }

        /**
         * Draw the Planet Notches to Show Where they Occur
         *
         * NOTE: We also make a note of where they are being plotted so we can do draw the aspects later.
         */
        foreach ($this->_planets as $planet => $details) {

            // Skip over planets we want to ignore
            if (in_array($planet, $this->ignorePlanets)) continue;

            // Find the angle of the sign
            $planetRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
            $planetAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $planetRelativeAngle);

            // draw a line to mark its place
            $this->drawPartialPolarLine(
                $planetAbsoluteAngle,
                $this->starSignNotchOuterRadius - $this->starSignNotchSize,
                $this->starSignNotchSize,
                $this->starSignNotchColor
            );

            // Record the Planet Positions for Drawing Aspects... Record the Position at the Outer Radius of the Aspect Circle
            $this->planetPositions[$planet]['x'] = $this->getPolarX($planetAbsoluteAngle, $this->aspectOuterRadius);
            $this->planetPositions[$planet]['y'] = $this->getPolarY($planetAbsoluteAngle, $this->aspectOuterRadius);

            // Record the Angles so we can later do Collision Detection and Draw the Glyphs in the right spot
            $this->planetAngles[$planet] = $planetAbsoluteAngle;
            $this->planetGlyphAngles[$planet] = $planetAbsoluteAngle;
        }

        /**
         * Draw in the Aspects
         */
        foreach ($this->_aspects as $aspect) {

            if ( (in_array($aspect['Planet1'], $this->ignoreAspects)) || (in_array($aspect['Planet2'], $this->ignoreAspects))) continue;

            if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'line') {

                AdGuru_Image_Toolbox::imageSmoothAlphaLine(
                    $this->image,
                    $this->planetPositions[$aspect['Planet1']]['x'],
                    $this->planetPositions[$aspect['Planet1']]['y'],
                    $this->planetPositions[$aspect['Planet2']]['x'],
                    $this->planetPositions[$aspect['Planet2']]['y'],
                    $this->aspectStyles[$aspect['Aspect']]['color']['R'], $this->aspectStyles[$aspect['Aspect']]['color']['G'], $this->aspectStyles[$aspect['Aspect']]['color']['B'], 0);

            } else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dashed') {

                $aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
                $aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
                $white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
                $style = array($aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white, $white);
                imagesetstyle($this->image, $style);
                imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

            } else if ($this->aspectStyles[$aspect['Aspect']]['style'] == 'dotted') {

                $aspectColor = $this->aspectStyles[$aspect['Aspect']]['color'];
                $aspectColorRGB = imagecolorallocate($this->image, $aspectColor['R'], $aspectColor['G'], $aspectColor['B']);
                $white = imagecolorallocate($this->image, 0xFF, 0xFF, 0xFF);
                $style = array($aspectColorRGB, $aspectColorRGB, $white, $white, $white, $white);
                imagesetstyle($this->image, $style);
                imageline($this->image, $this->planetPositions[$aspect['Planet1']]['x'], $this->planetPositions[$aspect['Planet1']]['y'], $this->planetPositions[$aspect['Planet2']]['x'], $this->planetPositions[$aspect['Planet2']]['y'], IMG_COLOR_STYLED);

            }

        }

        /**
         * Display House Positions, MC and Ascendant
         *
         * TODO: Add thickness to antialiased lines..
         */
        $houseCounter = 1;
        foreach ($this->_houses as $house => $details) {

            // Find the angle of the house
            $houseRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
            $houseAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $houseRelativeAngle);

            // Draw the Ascendant, Descendant, MC (MidHeaven) and IC
            if ( ($house == 'MC (Midheaven)') || ($house == 'Ascendant') ) {

                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
                $this->drawCenteredText($houseAbsoluteAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($house)], $this->acMcGlyphFontSize, $this->acMcColor );
                $this->drawCenteredText($houseAbsoluteAngle - $this->acMcTextOffset, $this->acMcTextDistance, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

                $oppAngle = $houseAbsoluteAngle - 180;
                if ($oppAngle <= 0) $oppAngle += 360;
                $oppGlyph = ($house == 'Ascendant' ? $this->signFont['descendant'] : $this->signFont['ic']);

                // Not sure why or how this works while it's commented out... but it does, otherwise we get a thick line for the DC.
                /*
                $this->drawPartialPolarLine($oppAngle, $this->starSignOuterRadius, $this->acMcNotchSize, $this->notchColor);
                $this->drawPartialPolarLine($oppAngle, $this->centerOuterRadius, $this->starSignNotchOuterRadius - $this->centerOuterRadius, $this->notchColor);
                */
                $this->drawCenteredText($oppAngle, $this->acMcGlyphDistance, $this->hamburgSymbolsPath, $oppGlyph, $this->acMcGlyphFontSize, $this->acMcColor );
                $this->drawCenteredText($oppAngle - $this->acMcTextOffset, $this->acMcTextDistance, $this->arialPath, $details['deg'] . "'" . $details['min'], $this->acMcTextFontSize, $this->acMcColor);

                // Record House Position for Plotting Aspects...
                $planetPositionName = ($house == 'Ascendant' ? 'Ascendant' : 'Midheaven');
                $this->planetPositions[$planetPositionName]['x'] = $this->getPolarX($houseAbsoluteAngle, $this->aspectOuterRadius);
                $this->planetPositions[$planetPositionName]['y'] = $this->getPolarY($houseAbsoluteAngle, $this->aspectOuterRadius);


            // Draw Standard House Divider
            } else {
                $this->drawPartialPolarLine($houseAbsoluteAngle, $this->centerOuterRadius, $this->planetOuterRadius - $this->centerOuterRadius, $this->notchColor);
            }

            // Record the House Angle in the House Text Array so we can draw the Numbers(Text) later
            $this->houseAngles[$houseCounter] = $houseAbsoluteAngle;

            $houseCounter++;

        }

        /**
         * Draw the Numbers in between the House Lines now that we have each House Angle.
         */
        foreach ($this->houseAngles as $houseNumber => $currentAngle) {

            $nextAngle = ($houseNumber < 12 ? $this->houseAngles[$houseNumber + 1] : $this->houseAngles[1]);
            $diff = ($currentAngle > $nextAngle ? $currentAngle - $nextAngle : ($currentAngle + 360) - $nextAngle);

            $newAngle = $currentAngle - ($diff / 2);

            if ($newAngle <= 0) {
            	$newAngle += 360;
            }

            $this->drawCenteredText($newAngle, $this->houseTextDistance, $this->arialPath, $houseNumber, $this->houseTextFontSize, $this->notchColor);
        }

        /**
         * Now Do Collision Detection for the Planet Glyphs
         */
        $houseCollisions = array();
        foreach ($this->_planets as $planet => $details) {

            // Skip over planets we want to ignore
            if (in_array($planet, $this->ignorePlanets)) continue;

            foreach ($this->houseAngles as $houseNumber => $houseAngle) {

                $diff = ( ($this->planetGlyphAngles[$planet] - $houseAngle) < 0 ? ($this->planetGlyphAngles[$planet] - $houseAngle) * -1 : ($this->planetGlyphAngles[$planet] - $houseAngle));

                // 5 is the angle difference, and then it is used later as well.
                if ($diff <= $this->planetCollisionLimit) {

                    //echo '<br />Planet: ' . $planet . ' is hitting house: ' . $houseNumber . '. The planet angle is: ' . $this->planetGlyphAngles[$planet] . ' and the house angle is: ' . $houseAngle . '<br />';
                    $houseCollisions[$houseNumber][$planet] = $this->planetGlyphAngles[$planet];
                    //var_dump($planet);
                    //var_dump($this->planetGlyphAngles[$planet]);
                    //var_dump($houseAngle);

                    $newAngle = ( ($this->planetGlyphAngles[$planet] - $houseAngle) < 0 ? $houseAngle - $this->planetCollisionLimit : $houseAngle + $this->planetCollisionLimit);

                    if ($newAngle <= 0) {
                    	$newAngle += 360;
                    }

                    $sameAngle = array_search($newAngle, $this->planetGlyphAngles);
                    while ($sameAngle !== false) {
                        $newAngle = ( ($this->planetGlyphAngles[$planet] - $houseAngle) < 0 ? $this->planetGlyphAngles[$sameAngle] - $this->planetCollisionLimit : $this->planetGlyphAngles[$sameAngle] + $this->planetCollisionLimit);
                        $sameAngle = array_search($newAngle, $this->planetGlyphAngles);
                    }

                    //var_dump($newAngle);

                    // Update Planet Glyph Array
                    $this->planetGlyphAngles[$planet] = $newAngle;

                    /**
                     * This seems to make it worse... but I'm sure it needs to happen...
                     */
                    //$this->_planetsLocked[] = $planet;

                    //echo 'Planet: ' . $planet . ' locked against house; <br />';

                }

            }

        }

        //var_dump($houseCollisions);
        foreach ($houseCollisions as $houseNumber => $collisionArray) {
        	asort($collisionArray);

        	$counter = 0;
        	foreach ($collisionArray as $planet => $planetAngle) {

        	    if ( ($counter == 0) && (sizeof($collisionArray) == 1)) {
        	    	// Check to see if it is bordering on any house boundary... and if it is lock it
        	    	//echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house... lock it.<br />';
                    $this->_planetsLocked[] = $planet;
        	    } else if ($counter == 0) {
        	    	$tmpHouseNum = ($houseNumber == 12 ? 1 : $houseNumber + 1);
                    $diff = ( ($planetAngle - $this->houseAngles[$tmpHouseNum]) < 0 ? ($planetAngle - $this->houseAngles[$tmpHouseNum]) * -1 : ($planetAngle - $this->houseAngles[$tmpHouseNum]));
                    //echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the first one?<br />';

                    // 5 is the angle difference, and then it is used later as well.
                    if ($diff <= $this->planetCollisionLimit) {
                        //echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the first one?... lock it.<br />';
                        $this->_planetsLocked[] = $planet;
                    }

                } else if ( ($counter == sizeof($collisionArray) - 1) ) {
                    $diff = ( ($planetAngle - $this->houseAngles[$houseNumber]) < 0 ? ($planetAngle - $this->houseAngles[$houseNumber]) * -1 : ($planetAngle - $this->houseAngles[$houseNumber]));
                    //echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') the diff is ' . $diff . ' is it the last one?<br />';

                    // 5 is the angle difference, and then it is used later as well.
                    if ($diff <= $this->planetCollisionLimit) {
                        //echo 'Planet: ' . $planet . ' at angle ' . $planetAngle . 'is bordering on a house (' . $this->houseAngles[$tmpHouseNum] . ') is it the last one?... lock it.<br />';
                        $this->_planetsLocked[] = $planet;
                    }

        	    }

        	    $counter++;

        	}

        }

        asort($this->planetGlyphAngles);
        //var_dump($this->planetGlyphAngles);

        $kSortedPlanets = array();
        foreach ($this->planetGlyphAngles as $planet => $angle) {
        	$kSortedPlanets[] = array('planet' => $planet, 'angle' => $angle);
        }

        foreach ($kSortedPlanets as $planetNumber => $planetDetails) {

            $planetAngle = $planetDetails['angle'];
            $nextPlanetId = ($planetNumber < (sizeof($kSortedPlanets)-1) ? $planetNumber + 1 : 0);
            $nextAngle = $kSortedPlanets[$nextPlanetId]['angle'];

            $diff = ( ($nextAngle - $planetAngle) < 0 ? ($nextAngle - $planetAngle) * -1 : ($nextAngle - $planetAngle));

            if ($diff <= $this->planetCollisionLimit) {

                //echo '<br />This planet: ' . $planetDetails['planet'] . '<br />';
                //var_dump($this->_planetsLocked);
                //echo '<br />';
                if (in_array($planetDetails['planet'], $this->_planetsLocked)) {
                    //echo 'Next Planet: ' . $kSortedPlanets[$nextPlanetId]['planet'] . ' is being moved<br />';
                    $kSortedPlanets[$nextPlanetId]['angle'] += $this->planetCollisionLimit;
                    $this->_planetsLocked[] = $kSortedPlanets[$nextPlanetId]['planet'];
                    //echo 'Planet: ' . $planetDetails['planet'] . ' locked; <br />';
                } else if (in_array($kSortedPlanets[$nextPlanetId]['planet'], $this->_planetsLocked)) {
                    //echo 'Next Planet: ' . $kSortedPlanets[$nextPlanetId]['planet'] . ' is locked<br />';
                    $kSortedPlanets[$planetNumber]['angle'] -= $this->planetCollisionLimit;
                } else {
                    //echo 'This planet: ' . $kSortedPlanets[$planetNumber]['planet'] . ' and Next Planet: ' . $kSortedPlanets[$nextPlanetId]['planet'] . ' are being moved<br />';
                    $kSortedPlanets[$planetNumber]['angle'] -= $this->planetCollisionLimit/2;
                    $kSortedPlanets[$nextPlanetId]['angle'] += $this->planetCollisionLimit/2;
                }
                //echo 'This planets resulting angle: ' . $kSortedPlanets[$planetNumber]['angle'] . '<br />';
                //echo 'Next planets resulting angle: ' . $kSortedPlanets[$nextPlanetId]['angle'] . '<br />';

            }

        }

        /**
         * Draw the Planet Glyphs now that they've been adjusted.
         */
        foreach ($kSortedPlanets as $planetDetails) {
            $this->drawCenteredText($planetDetails['angle'], $this->planetGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($planetDetails['planet'])], $this->planetGlyphFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetDegTextDistance, $this->arialPath, $this->_planets[$planetDetails['planet']]['deg'], $this->planetTextFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetSignGlyphDistance, $this->hamburgSymbolsPath, $this->signFont[strtolower($this->_planets[$planetDetails['planet']]['sign'])], $this->planetSignGlyphFontSize, $this->planetColor);
            $this->drawCenteredText($planetDetails['angle'], $this->planetMinTextDistance, $this->arialPath, $this->_planets[$planetDetails['planet']]['min'], $this->planetTextFontSize, $this->planetColor);
        }

        //header( 'Content-Type: image/png' );
        if (is_null($filename)) {
            $filename = 'chart-' . mktime() . rand(1,20) . '.png';
            imagejpeg($this->image, $this->tempImagePath . $filename);
        } else {
            imagepng($this->image, $this->tempImagePath . $filename);
        }

        return $filename;
    }

    public function drawAspectChart($filename = null) {

        $this->chartHeight = 1010;
        $this->chartWidth = 930;

        $squareSize = 72;

        $planetCounter = 0;
        foreach ($this->_planets as $planetName => $planetDetails) {
            if (!in_array($planetName, $this->ignorePlanets)) {
            	$planets[$planetCounter] = array_merge(array('name' => $planetName), $planetDetails);
            	$rPlanets[strtolower($planetName)] = $planetCounter;
            	$planetCounter++;
            }
        }

        $rPlanets['ascendant'] = sizeof($rPlanets);
        $rPlanets['midheaven'] = sizeof($rPlanets);

        $planetAspects = array();
        foreach ($this->_aspects as $aspectDetails) {

            if (!in_array($aspectDetails['Planet1'], $this->ignorePlanets) && !in_array($aspectDetails['Planet2'], $this->ignorePlanets) && $rPlanets[strtolower($aspectDetails['Planet1'])] < $rPlanets[strtolower($aspectDetails['Planet2'])]  ) {
            	$planetAspects[strtolower($aspectDetails['Planet1'])][strtolower($aspectDetails['Planet2'])] = array_merge($aspectDetails, array('Planet1Position' => $rPlanets[strtolower($aspectDetails['Planet1'])], 'Planet2Position' => $rPlanets[strtolower($aspectDetails['Planet2'])]));

            } elseif ( (($aspectDetails['Planet2'] == 'Ascendant') || ($aspectDetails['Planet2'] == 'Midheaven')) && ($rPlanets[strtolower($aspectDetails['Planet1'])] < $rPlanets[strtolower($aspectDetails['Planet2'])]) ) {
            	$planetAspects[strtolower($aspectDetails['Planet1'])][strtolower($aspectDetails['Planet2'])] = array_merge($aspectDetails, array('Planet1Position' => $rPlanets[strtolower($aspectDetails['Planet1'])], 'Planet2Position' => $rPlanets[strtolower($aspectDetails['Planet2'])]));
            }

        }

        $numberPlanets = sizeof($planets);

        $xOffset = 0;
        $yOffset = 72;

        $this->image = imageCreateTrueColor( $this->chartWidth, $this->chartHeight );
        imagefilledrectangle($this->image, 0, 0, $this->chartWidth, $this->chartHeight, imagecolorallocate($this->image, 255, 255, 255));

        foreach ($planets as $counter => $planetDetails) {

            /**
             * Draw Glyph at top of column
            **/
            $textBox = @imagettfbbox(40,0,$this->hamburgSymbolsPath,$this->signFont[strtolower($planetDetails['name'])]);
            $textWidth = abs($textBox[4] - $textBox[0]);
            $textHeight = abs($textBox[5] - $textBox[1]);

            imagettftext($this->image,40,0,$counter * $squareSize + $squareSize/2 - ($textWidth/2),$counter * $squareSize + $squareSize/2 + ($textHeight/2),imagecolorallocate($this->image, 0x00, 0x00, 0x00),$this->hamburgSymbolsPath,$this->signFont[strtolower($planetDetails['name'])]);

            /**
             * Draw Column of boxes
             */
            $defaultX = $counter * $squareSize + $xOffset;
            $defaultY = $counter * $squareSize + $yOffset;

            // Draw Left Border
            imageline($this->image, $defaultX,$defaultY,$defaultX,(($numberPlanets + 1) * $squareSize) + $yOffset,imagecolorallocate($this->image, 0, 0, 0)); // Left border

            // Draw Right Border (Only SquareSize)
            imageline($this->image, $defaultX + $squareSize,$defaultY,$defaultX + $squareSize,$defaultY + ($squareSize * 2),imagecolorallocate($this->image, 0, 0, 0));

            // Draw Rungs
            for ($i = 0; $i <= ($numberPlanets - $counter + 1); $i++) {
                imageline($this->image, /* x1 */$counter * $squareSize + $xOffset,/* y1 */($counter * $squareSize + $yOffset) + ($i * $squareSize),/* x2 */($counter + 1) * $squareSize + $xOffset,/* y2 */($counter * $squareSize + $yOffset) + ($i * $squareSize),imagecolorallocate($this->image, 0, 0, 0));
            }

            /**
             * Find Aspects for this planet and draw below
             */
            foreach ($planetAspects[strtolower($planets[$counter]['name'])] as $planet2 => $aspectDetails) {

                $character = $this->signFont[strtolower($aspectDetails['Aspect'])];

                $textBox = @imagettfbbox(32,0,$this->hamburgSymbolsPath,$character);
                $textWidth = abs($textBox[4] - $textBox[0]);
                $textHeight = abs($textBox[5] - $textBox[1]);

                $x = $counter * $squareSize + $squareSize/2 - ($textWidth/2);
                $y = ($aspectDetails['Planet2Position'] - 1) * $squareSize + $yOffset + $squareSize/2 + ($textHeight/2);

                imagettftext($this->image,32,0,$x,$y,imagecolorallocate($this->image, $this->aspectStyles[$aspectDetails['Aspect']]['color']['R'], $this->aspectStyles[$aspectDetails['Aspect']]['color']['G'], $this->aspectStyles[$aspectDetails['Aspect']]['color']['B']),$this->hamburgSymbolsPath,$character);
            }


        }

        $counter++;

        /**
         * Special Rules for Ac/Mc -- Draw Glyphs
        **/
        $textBox = @imagettfbbox(40,0,$this->hamburgSymbolsPath,$this->signFont['ascendant']);
        $textWidth = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);

        imagettftext($this->image,40,0,$counter * $squareSize + $squareSize/2 - ($textWidth/2),$counter * $squareSize + $squareSize/2 + ($textHeight/2),imagecolorallocate($this->image, 0x00, 0x00, 0x00),$this->hamburgSymbolsPath,$this->signFont['ascendant']);

        $textBox = @imagettfbbox(40,0,$this->hamburgSymbolsPath,$this->signFont['mc (midheaven)']);
        $textWidth = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);
        imagettftext($this->image,40,0,$counter * $squareSize + $squareSize/2 - ($textWidth/2),($counter + 1) * $squareSize + $squareSize/2 + ($textHeight/2),imagecolorallocate($this->image, 0x00, 0x00, 0x00),$this->hamburgSymbolsPath,$this->signFont['mc (midheaven)']);


        if (is_null($filename)) {
            $filename = 'tri-chart-' . mktime() . rand(1,20) . '.png';
            imagejpeg($this->image, $this->tempImagePath . $filename);
        } else {
            imagepng($this->image, $this->tempImagePath . $filename);
        }

        return $filename;		
	}
	
	
	public function initializeChart() {
        $this->chartWidth = 1200;
        $this->chartHeight = $this->chartWidth;

        $this->hamburgSymbolsPath = Zend_Registry::get('configuration')->fonts->path . 'HamburgSymbols.ttf';
        $this->arialPath = Zend_Registry::get('configuration')->fonts->path . 'arial.ttf';
        
        $this->tempImagePath = Zend_Registry::get('configuration')->temp->path;
        
        $this->acMcOuterRadius = $this->chartWidth/2;
        $this->acMcNotchOuterRadius = $this->acMcOuterRadius - 50;
        $this->starSignOuterRadius = $this->acMcNotchOuterRadius - ($this->notchSize2 * 2);
        $this->starSignNotchOuterRadius = $this->starSignOuterRadius - 100;
        $this->planetOuterRadius = $this->starSignNotchOuterRadius - ($this->notchSize2 * 2);
        $this->aspectOuterRadius = $this->planetOuterRadius - 150;
        $this->centerOuterRadius = 50;

        $this->acMcBackgroundColor = array( 'R' => 0xCD, 'G' => 0xE3, 'B' => 0xCA );
        $this->starSignBackgroundColor = array( 'R' => 0xAD, 'G' => 0xE3, 'B' => 0xA6 );
        $this->planetBackgroundColor = array( 'R' => 0xCA, 'G' => 0xCE, 'B' => 0xE3 );
        $this->aspectBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
        $this->centerBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
        $this->notchBackgroundColor = array( 'R' => 0xFF, 'G' => 0xFF, 'B' => 0xFF );
        $this->starSignNotchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0xFF );
        $this->notchColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );
        $this->strokeColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );

        $this->starSignColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );
        $this->planetColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );
        $this->acMcColor = array( 'R' => 0x00, 'G' => 0x00, 'B' => 0x00 );

        $this->ignorePlanets = array('Lilith', 'P. of Fortune', 'Vertex');
        $this->ignoreAspects = array('Ascendant', 'Chiron', 'Midheaven', 'Lilith', 'P. of Fortune', 'Vertex', 'True Node');

        $this->aspectStyles = array(
            'Square' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Trine' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Conjunction' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Quincunx' => array('style' => 'dashed', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
            'Sextile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Semisextile' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0xFF, 'B' => 0x00)),
            'Semisquare' => array('style' => 'dotted', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
            'Opposition' => array('style' => 'line', 'color' => array('R' => 0xFF, 'G' => 0x00, 'B' => 0x00)),
            'Sesquiquare' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0xFF)),
            'Quintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
            'Biquintile' => array('style' => 'line', 'color' => array('R' => 0x00, 'G' => 0x00, 'B' => 0x00)),
        );

        $this->acMcGlyphDistance = $this->acMcOuterRadius - 20;
        $this->acMcTextDistance = $this->acMcOuterRadius - 25;

        $this->houseTextDistance = $this->centerOuterRadius + 25;

        $this->planetGlyphDistance = $this->planetOuterRadius - 35;
        $this->planetMinTextDistance = $this->planetGlyphDistance - 35;
        $this->planetSignGlyphDistance = $this->planetMinTextDistance - 25;
        $this->planetDegTextDistance = $this->planetSignGlyphDistance - 25;		
	}
	
	public Function convertLongitudeToArray($longitude) {
		$signs = array (0 => 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces');
		
		$sign_num = floor($longitude / 30);
		$pos_in_sign = $longitude - ($sign_num * 30);
		$deg = floor($pos_in_sign);
		$full_min = ($pos_in_sign - $deg) * 60;
		$min = floor($full_min);
		$full_sec = round(($full_min - $min) * 60);
		
		if ($deg < 10)
		{
		  $deg = "0" . $deg;
		}
		
		if ($min < 10)
		{
		  $min = "0" . $min;
		}
		
		if ($full_sec < 10)
		{
		  $full_sec = "0" . $full_sec;
		}
		
		return array('deg' => $deg, 'sign' => $signs[$sign_num], 'min' => $min, 'sec' => $full_sec);
	}
	
	public function mid($midstring, $midstart, $midlength) {
		return(substr($midstring, $midstart-1, $midlength));
	}	
	
	/**
	 * Return Birth Date
	 *
	 * @param String $format
	 * @return Zend_Date
	 */
	public function getBirthDate($format = null) {

		/**
		 * Lazy Load the Zend_Date object
		 */
		if (!($this->_birthDate instanceof Zend_Date) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date'])) && (isset($this->_data['birth_time']) && !empty($this->_data['birth_time']))) {
			$this->_birthDate = new Zend_Date($this->_data['birth_date']['year'] . '-' . $this->_data['birth_date']['month'] . '-' . $this->_data['birth_date']['day'] . ' ' . $this->_data['birth_time']['hour'] . ':' . $this->_data['birth_time']['minute'] . ' ' . $this->_data['birth_time']['meridiem']);
		} else if(!($this->_birthDate instanceof Zend_Date) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date']))) {
			$this->_birthDate = new Zend_Date($this->_data['birth_date']);
		}
		
		if ($this->_birthDate instanceof Zend_Date) {
			
			if (is_null($format))
				return $this->_birthDate;
			
			return $this->_birthDate->toString($format);
		}
		
		return false;
		
	}
		
	/**
	 * Return Location
	 *
	 * @return Acs_Location_Row
	 */
	public function getLocationRow() {
		
		if (!$this->_location instanceof Acs_Location_Row) {
				
			$locationTable = new Acs_Location_Table();
			$select = $locationTable->select()->where('id = ?', $this->_data['acs_location_id']);
			$row = $locationTable->fetchRow($select);
			
			if ($row instanceof Acs_Location_Row)
				$this->_location = $row;
		}

		if ($this->_location instanceof Acs_Location_Row)
			return $this->_location;
		
		return null;
				
	}
	
	/**
	 * Return Timezone
	 *
	 * @return Acs_Timezone
	 */
	public function getTimezone() {
		
		if (!$this->_timezone instanceof Acs_Timezone) {
				
			$tz = new Acs_Timezone($this->getLocationRow(), $this->getBirthDate());
			
			if ($tz instanceof Acs_Timezone)
				$this->_timezone = $tz;
		}

		if ($this->_timezone instanceof Acs_Timezone)
			return $this->_timezone;
		
		return null;
				
	}	
	
	public function getFirstName() {
		return (isset($this->_data['current_first_name']) && !empty($this->_data['current_first_name']) ? $this->_data['current_first_name'] : '');
	}
	
	public function getLastName() {
		return (isset($this->_data['current_last_name']) && !empty($this->_data['current_last_name']) ? $this->_data['current_last_name'] : '');
	}
	
	public function getBirthFullName() {
		return (isset($this->_data['birth_full_name']) && !empty($this->_data['birth_full_name']) ? $this->_data['birth_full_name'] : '');
	}
	
	
    /**
     * Sets all data from an array.
     *
     * @param  array $data
     */
    public function setFromArray(array $data)
    {
    	$defaults = array_combine(array_keys($this->_data), array_fill(0, count($this->_data), null));
        $data = array_intersect_key($data, $defaults);
    	
        foreach ($data as $columnName => $value) {
            $this->__set($columnName, $value);
        }

    }	
    
    /**
     * Set data field value
     *
     * @param  string $columnName The column key.
     * @param  mixed  $value      The value for the property.
     * @return void
     * @throws Zend_Exception
     */
    public function __set($columnName, $value)
    {
        if (!array_key_exists($columnName, $this->_data)) {
            throw new Zend_Exception("Specified column \"$columnName\" is not in the row");
        }
        $this->_data[$columnName] = $value;
    }    
    
	function sortArrayByField
        (
            $original,
            $field,
            $descending = false
        )
        {
            $sortArr = array();

            foreach ( $original as $key => $value )
            {
                $sortArr[ $key ] = $value[ $field ];
            }

            if ( $descending )
            {
                arsort( $sortArr );
            }
            else
            {
                asort( $sortArr );
            }

            $resultArr = array();
            foreach ( $sortArr as $key => $value )
            {
                $resultArr[ $key ] = $original[ $key ];
            }

            return $resultArr;
        }
            
    function drawPartialPolarLine($angle, $start, $length, $color) {

        $angle = ($angle == 0 ? 360 : $angle);

        $startx = $this->chartWidth/2 + sin( deg2rad( $angle ) )  * $start;
        $starty = $this->chartWidth/2 + cos( deg2rad( $angle + 180 ) ) * $start;

        $endx = $startx + sin( deg2rad( $angle ) )  * $length;
        $endy = $starty + cos( deg2rad( $angle + 180 ) ) * $length;

        if ( ($angle == 90) || ($angle == 180) || ($angle == 270) || ($angle == 360) ) {
            imageline($this->image, $startx, $starty, $endx, $endy, $color);
        } else {
            AdGuru_Image_Toolbox::imageSmoothAlphaLine($this->image, $startx, $starty, $endx, $endy, $color['R'], $color['G'], $color['B'], 0);
        }


    }

    function drawCenteredText($angle, $distance, $font, $text, $size, $color = array('R' => 0x00, 'G' => 0x00, 'B' => 0x00) ) {

    	$textBox = imagettfbbox($size,0,$font,$text);
        $textWidth = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);

        imagettftext(
            $this->image,
            $size,
            0,
            $this->getPolarX($angle, $distance) - ($textWidth/2),
            $this->getPolarY($angle, $distance) + ($textHeight/2),
            imagecolorallocate($this->image, $color['R'], $color['G'], $color['B']),
            $font,
            $text);

    }

    function drawCenteredCircle($radius, $color, $stroke = 1) {

        if ($stroke > 0) {
            AdGuru_Image_Toolbox::imageSmoothCircle( $this->image, $this->chartWidth/2,  $this->chartHeight/2,  $radius+1, $this->strokeColor);
            AdGuru_Image_Toolbox::imageSmoothCircle( $this->image, $this->chartWidth/2,  $this->chartHeight/2,  $radius, $color);
        } else {
            AdGuru_Image_Toolbox::imageSmoothCircle( $this->image, $this->chartWidth/2,  $this->chartHeight/2,  $radius, $color);
        }

    }

    function getPolarX($angle, $distance) {
        $result = $this->chartWidth/2 + sin( deg2rad( $angle ) )  * $distance;

        return $result;

    }

    function getPolarY($angle, $distance) {
        $result = $this->chartWidth/2 + cos( deg2rad( $angle + 180 ) )  * $distance;

        return $result;
    }
    
}
?>