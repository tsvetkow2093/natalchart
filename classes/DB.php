<?php
	class DB {
		protected $errorMessage = null;
		protected $mySQLi = null;
		
		public function __construct(  ) {
			
		}		
		
		public function getDBConnection(  ) {
			if( !$this->getMySQLi(  ) ) {
				$mySQLi = mysqli_connect( Config::HOST_NAME, Config::DB_USER, Config::DB_PASSWORD, Config::DB_NAME );
				
				if( mysqli_connect_errno(  ) ) {
					$this->errorMessage = "Failed to connect to MySQL: (" . mysqli_connect_error(  ) . ").";
				}
				
				$charset = "utf8";
				$setCharset = mysqli_set_charset( $mySQLi, $charset );
				
				if( !$setCharset ) {
					$this->errorMessage = "Error when loading a set of characters utf8: " . mysqli_error( $mySQLi );
				}
				
				if( !$this->getErrorMessage(  ) ) {
					$this->mySQLi = $mySQLi;
				}
			}
			
			return $this->getMySQLi(  );
		}		

		public function executeQuery( $query ) {
			if( !$this->getMySQLi(  ) ) {
				$this->getDBConnection(  );
			}
			
			$queryResult = mysqli_query( $this->getMySQLi(  ), $query );

			if( !$queryResult ) {
				$this->errorMessage = "Failed to execute the MySQL-query!";
				
				return null;
			}

			return $queryResult;
		}
		
		public function getErrorMessage(  ) {
			return $this->errorMessage;
		}
		
		public function getMySQLi(  ) {
			return $this->mySQLi;
		}

		public function closeMySQLiConnection(  ) {
			return mysqli_close( $this->mySQLi );
		}
	}
?>