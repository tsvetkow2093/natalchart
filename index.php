<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
set_time_limit(0);
date_default_timezone_set('UTC');

require_once( "classes/Config.php" );
require_once( "classes/DB.php" );
require_once( "classes/AdGuru/Image/Toolbox.php" );
require_once( "classes/Acs/Location/Row.php" );
require_once( "classes/Acs/Location/Table.php" );
require_once( "classes/Acs/Timezone/Row.php" );
require_once( "classes/Acs/Timezone/Table.php" );
require_once( "classes/Acs/Timezone.php" );
require_once( "classes/Abstract.php" );
require_once( "classes/NatalChart.php" );
require_once( "classes/AdaptedNatalChart.php" );
require_once( "classes/RelocationChart.php" );
require_once( "classes/AdaptedToolbox.php" );

$cookies = (isset($_COOKIE['anc-details'])) ? json_decode(base64_decode($_COOKIE['anc-details'], true)) : false;
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Personalized Natal Chart</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
<style type="text/css">
	.easy-autocomplete-container ul{ height: 150px; overflow: hidden; overflow-y: auto; }
	#natal-chart-image{ max-width: 600px; margin: 50px auto; }
	.report-paragraph{ padding: 30px 15px; margin-bottom: 30px;  }
	.page-title{ font-size: 16px; font-weight: 600; margin-top: 50px; }
	.page-subtitle{ font-size: 12px; font-weight: 400; margin: 5px auto; }
	.print{ position: absolute; display: block: width: 22px; top: 0; right: 0; }
	@media print {
		.print { display: none; }
		.social_ico { display: none; }
		.sendModal { display: none; }
		.alert { display: none; }
	}
</style>
</head>
<body>

<div id="form-container" class="container">
  	<div class="row">
	  	<div class="col-xs-12">
		  	<div class="page-header text-center">
		    	<h1>Get Your Personalized Natal Chart Now, 100% Free:</h1>
		  	</div>
	  	</div>
	</div>

	<div class="row">
	  	<div class="col-xs-12">
		  	<form id="natal-chart-form" class="form-horizontal">

				<div class="form-group">
					<label for="current_first_name" class="col-sm-3 control-label">Your First Name:</label>
					<div class="col-sm-7">
						<input type="text" id="current_first_name" name="current_first_name" class="form-control" placeholder="Your First Name" value="<?php echo ($cookies) ? $cookies->current_first_name : '';?>">
					</div>
				</div>

				<div class="form-group">
					<label for="current_last_name" class="col-sm-3 control-label">Your Last Name:</label>
					<div class="col-sm-7">
						<input type="text" id="current_last_name" name="current_last_name" class="form-control" placeholder="Your Last Name" value="<?php echo ($cookies) ? $cookies->current_last_name : '';?>">
					</div>
				</div>

				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Date of birth:</label>
					<div class="col-sm-2">
						<select id="birth_date_month" name="birth_date_month" class="form-control">
		                    <option label="Month" value="" <?php echo (!$cookies) ? 'selected' : '';?>>Month</option>
		                    <?php for ($i=1; $i <= 12; $i++): ?>
		                      <option label="<?php echo DateTime::createFromFormat( '!m', sprintf("%02d", $i) )->format('F');?>" value="<?php echo $i;?>" <?php echo ($cookies && $cookies->birth_date_month == $i) ? 'selected' : '';?>><?php echo DateTime::createFromFormat( '!m', sprintf("%02d", $i) )->format('F');?></option>
		                    <?php endfor; ?>
		                </select>
		            </div>
		            <div class="col-sm-2">
		                <select id="birth_date_day" name="birth_date_day" class="form-control">
		                    <option label="Day" value="" <?php echo (!$cookies) ? 'selected' : '';?>>Day</option>
		                    <?php for ($i=1; $i <= 31; $i++): ?>
		                      <option label="<?php echo $i;?>" value="<?php echo $i;?>" <?php echo ($cookies && $cookies->birth_date_day == $i) ? 'selected' : '';?>><?php echo $i;?></option>
		                    <?php endfor; ?>
		                </select>
		            </div>
		            <div class="col-sm-2">
						<select id="birth_date_year" name="birth_date_year" class="form-control">
							<option label="Year" value="" <?php echo (!$cookies) ? 'selected' : '';?>>Year</option>
							<?php for ($i=date('Y') - 100; $i <= date('Y') - 5; $i++): ?>
								<option label="<?php echo $i;?>" value="<?php echo $i;?>" <?php echo ($cookies && $cookies->birth_date_year == $i) ? 'selected' : '';?>><?php echo $i;?></option>
							<?php endfor; ?>
						</select>
		            </div>
				</div>

				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Time of birth:</label>
					<div class="col-sm-2">
						<select id="birth_time_hour" name="birth_time_hour" class="form-control">
                      		<?php for ($i=1; $i <= 12; $i++): ?>
                        		<option label="<?php echo $i; ?>" value="<?php echo $i; ?>" <?php echo ($cookies && $cookies->birth_time_hour == $i) ? 'selected' : '';?> <?php echo (!$cookies && $i==12) ? 'selected' : '';?>><?php echo $i;?></option>
                      		<?php endfor; ?>
                  		</select>
                  	</div>
                  	<div class="col-sm-2">
                  		<select id="birth_time_minute" name="birth_time_minute" class="form-control">
                      		<?php for ($i=0; $i <= 59; $i++): ?>
                        		<option label="<?php echo sprintf("%02d", $i); ?>" value="<?php echo sprintf("%02d", $i); ?>" <?php echo ($cookies && $cookies->birth_time_minute == $i) ? 'selected' : '';?> <?php echo (!$cookies && $i==0) ? 'selected' : '';?>><?php echo sprintf("%02d", $i);?></option>
                      		<?php endfor; ?>
                  		</select>
              		</div>
              		<div class="col-sm-2">
						<select id="birth_time_meridiem" name="birth_time_meridiem" class="form-control">
							<option label="a.m." value="AM" <?php echo ($cookies && $cookies->birth_time_meridiem == 'AM') ? 'selected' : '';?>>a.m.</option>
							<option label="p.m." value="PM" <?php echo ($cookies && $cookies->birth_time_meridiem == 'PM') ? 'selected' : '';?> <?php echo (!$cookies) ? 'selected' : '';?>>p.m.</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
						<div class="checkbox">
							<label>
								<input type="checkbox" id="birth_time_unknown" name="birth_time_unknown" value="1"> I do not know my time of birth
							</label>
							<p style="max-width: 480px;"><small class="text-info">* For an unknown birth time we use the natural house system. This allocates the planets to their related zodiacal houses. The houses are of equal size and based on 0 degrees Aries ascendant or rising sign and each house cusp thereafter in zodiacal order.</small></p>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="current_last_name" class="col-sm-3 control-label">City of birth:</label>
					<div class="col-sm-7">
						<input type="text" id="start_city_search" name="start_city_search" class="form-control" value="<?php echo ($cookies) ? $cookies->start_city_search : '';?>" placeholder="City of birth">
						<p><small class="text-info">* Select from the options that appear as you type</small></p>
						<input type="hidden" id="start_acs_location_id" value="<?php echo ($cookies) ? $cookies->start_acs_location_id : '';?>" name="start_acs_location_id" >
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
						<button type="button" id="submit-natal-chart-form" class="btn btn-success">Prepare My Personalized Chart Now</button>
						<input type="hidden" name="action" value="getNatalChart">
					</div>
				</div>
			</form>
	  	</div>
	</div>

</div>

<div id="report-container" class="container" style="display: none;">
    <div class="alert alert-success hidden" id="success-alert">
        <div>Email send</div>
    </div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<h1 class="page-title">Natal Chart</h1>
			<h3 class="page-subtitle fullname-title"></h3>
			<h3 class="page-subtitle dob-title"></h3>
			<h3 class="page-subtitle city-title"></h3>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 text-center">
			<img id="natal-chart-image"/>
			<a onclick="print();" class="btn print">Print</a>
		</div>
        <div class="col-xs-12 text-center">
            <div class="canvas-natal-cart"></div>
        </div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<div class="social_ico">
				<button class="btn" onclick="window.open(twitterUrl, 'My Natal Chart', 'toolbar=0, status=0, width=548, height=325'); return false">Share Twitter</button>
				<button class="btn" onclick="window.open(facebookUrl, 'My Natal Chart', 'toolbar=0, status=0, width=548, height=325'); return false">Share Facebook</button>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 text-center">

			<a href="#sendModal" class="btn btn-primary sendModal" data-toggle="modal">Send to E-Mail</a>
			<div id="sendModal" class="modal fade">
				 <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title">Send to email</h4>
						</div>
						<div class="modal-body">
							<form role="form" class="form-horizontal">
								<div class="form-group has-feedback">
									<label for="email" class="control-label col-xs-3">Email:</label>
									<div class="col-xs-6">
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
											<input type="email" class="form-control" required="required" id="email" name="email">
										</div>
										<span class="glyphicon form-control-feedback"></span>
									</div>
								</div>
								<input type="hidden" name="action" value="sendNatalChart">
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="submit-email-form">Send</button>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<hr>
		<h1 class="text-center">Personality Profile</h1>
		<hr>
		<div id="natal-chart-report" class="col-xs-12 text-left"></div>
	</div>
</div>
<hr>
<style>
    @font-face {
        font-family: 'HamburgSymbols';
        src: local('HamburgSymbols'), url('fonts/HamburgSymbols.ttf');
    }
</style>
<section class="">
    <span style="font-family: 'HamburgSymbols'; font-weight:normal; font-size:1px">&#141;
    </span>
</section>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/additional-methods.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.4.0/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.3.3/jquery.appear.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/jquery.easy-autocomplete.js"></script>
<script type="text/javascript" src="assets/js/jcookies.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>

