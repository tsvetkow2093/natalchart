let twitterUrl = '';
let facebookUrl = '';

$(document).ready(function(){

    $("#start_city_search").easyAutocomplete({
      	url: function(phrase) { return "handler.php" },
        getValue: function(element) { return element.info; },
        ajaxSettings: { dataType: "json", method: "POST", data: { dataType: "json" } },
        preparePostData: function(data) { data.phrase = $("#start_city_search").val(); data.action = 'getLocation'; return data; },
		list: {
			match: true,
			maxNumberOfElements: 100,
			onChooseEvent: function(){
				var location = $("#start_city_search").getSelectedItemData();
				$('#start_acs_location_id').attr('value',location.id);
			}
		},
		requestDelay: 400
    });

	$.validator.setDefaults({ 
        ignore: "",
        errorPlacement: function(error, element){
            var name = element.attr("name");

            if(!$('#'+name+'-error').is(':visible'))
                var n = noty({
                    layout: 'bottomCenter',
                    timeout: 3000,
                    progressBar: true,
                    theme: 'metroui',
                    text: error,
                    maxVisible: 1,
                    force: true,
                    type: 'error',
                    animation: {
                        open: 'animated flipInX', // Animate.css class names
                        close: 'animated zoomOut', // Animate.css class names
                        easing: 'swing', // unavailable - no need
                        speed: 500 // unavailable - no need
                    }
                });
        }
    });

    $.validator.addMethod('username', function (value) { 
        return /^([a-zA-Z -']+)$/.test(value); 
    }, 'Only letters, space "`" and "- " are allowed.');

    $.validator.addMethod("nowhitespace", function(value, element)
    { return $.trim(value) && value != ""; }, "Can't be empty string");

    $("#natal-chart-form").validate({
        rules: {
            'current_first_name': { required: true, nowhitespace: true, username: true },
            'current_last_name': { required: true, nowhitespace: true, username: true },
            'birth_date_month': {required: true},
            'birth_date_day': {required: true},
            'birth_date_year': {required: true},
            'start_city_search': {required: true, nowhitespace: true},
            'start_acs_location_id': {required: true}
        },
        messages: {
            'current_first_name': { 'required' : 'Your First Name is required', 'nowhitespace': "First Name can't be empty string" },
            'current_last_name': { 'required' : 'Your Last Name is required', 'nowhitespace': "Last Name can't be empty string" },
            'birth_date_month': { 'required' : 'Your Birth Date Month is required'},
            'birth_date_day': { 'required' : 'Your Birth Date Day is required'}, 
            'birth_date_year': { 'required' : 'Your Birth Date Year is required'},
            'start_city_search': { 'required' : 'Your City of Birth is required', 'nowhitespace': "City of Birth can't be empty string"},
            'start_acs_location_id': { 'required' : 'Please select "City of birth" from the options that appear as you type'}
        }
    });

    $('#submit-natal-chart-form').on('click', function(){
    	if($("#natal-chart-form").valid()){
    		var dob = $('#birth_date_month').val()+'/'+$('#birth_date_day').val()+'/'+$('#birth_date_year').val();
            //validate dob for errors: ex. 31 of Februaby - is error, etc
            if(isDate(dob)){
                
                $('.fullname-title').text($('#current_first_name').val()+' '+$('#current_last_name').val())
                $('.dob-title').text(dob);
                $('.city-title').text($('#start_city_search').val());
                
                //set user astrology natal chart(anc) detail data cookies
                $.jCookies({ 
                	name : 'anc-details',
                	value : { 
                		current_first_name : $('#current_first_name').val(),
                		current_last_name : $('#current_last_name').val(),
                		birth_date_month : $('#birth_date_month').val(),
                		birth_date_day : $('#birth_date_day').val(),
                		birth_date_year : $('#birth_date_year').val(),
                		birth_time_hour : $('#birth_time_hour').val(),
                		birth_time_minute : $('#birth_time_minute').val(),
                		birth_time_meridiem : $('#birth_time_meridiem').val(),
                		start_city_search : $('#start_city_search').val(),
                		start_acs_location_id : $('#start_acs_location_id').val(),
                	}, 
                	days : 365 
                });
                
                getNatalChart();

            }else{

                //clear month and day values and validate form to show errors
                $('#birth_date_month').val('');
                $('#birth_date_day').val('');

                //validate form for error appears
                $('#user-details-form').valid();
            }
    	}
    });

    $('#submit-email-form').on('click', function(){
		var Valid = true;
		var formGroup = $('#email').parents('.form-group');
		var glyphicon = formGroup.find('.form-control-feedback');

		var pattern = /^[a-z0-9]([a-z0-9_\-\.]+)@[a-z0-9]([a-z0-9_\-\.]+)\.[a-z]{2,6}$/i;
		if($('#email').val() === ''){
			Valid = false;
		}
		if ($('#email').val().search(pattern) == 0) {
			formGroup.addClass('has-success').removeClass('has-error');
			glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
		} else {
			formGroup.addClass('has-error').removeClass('has-success');
			glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
			Valid = false;
		}
		if (Valid) {
			var data = $('#natal-chart-form').serialize();
			data += '&email='+$('#email').val();
			$('#sendModal').modal('hide');
			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: 'send.php',
				data: data,
				success: function(callback){
					$('#success-alert').removeClass('hidden');
					setTimeout(function(){
						
						$('#success-alert').addClass('hidden');
						
					}, 3000);
				}
			});
		}
    });
});


function getNatalChart(){
	var data = $('#natal-chart-form').serialize();

	$.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'handler.php',
        data: data,
        success: function(callback){
        	$('#form-container').fadeOut(1000,function(){
        		$('#natal-chart-image').attr('src', callback.image);
        		$('#natal-chart-report').html(callback.report);	
        		var rep = $.parseJSON(callback.report);
        		var report = '';
				twitterUrl = 'http://twitter.com/share?url='+encodeURIComponent(document.location.href+''+callback.image)+'&text='+encodeURIComponent('My Natal Chart: '+document.location.href+''+callback.image)+'&hashtags=Personalized,Natal,Chart';
				facebookUrl = 'http://www.facebook.com/sharer.php?u='+encodeURIComponent(document.location.href+''+callback.image)+'&title='+encodeURIComponent('My Natal Chart')+'&picture='+encodeURIComponent(document.location.href+''+callback.image)+'&quote='+encodeURIComponent('My Personalized Natal Chart')+'';

				$.each(rep.report.detail[0].section, function(i,e){
        			report += '<div class="bg-info report-paragraph">';
        			report += '<h3 class="text-center">'+e.title+'</h3>';
        			$.each(e.paragraph, function(k,p){
        				report += '<h5><b>'+p.heading+' <i>('+p.interaspect+')</i></b></h5>';
        				report += '<p>'+p.text+'</p>';
        			});
        			report += '</div>';
        		});

        		$('#natal-chart-report').html(report);
        		$('#report-container').fadeIn();

        		// данные карты с сервера
                var natalChart = $.parseJSON(callback.nc);
                // добавление канваса
                $('.canvas-natal-cart').html('<canvas id="c" width="1200" height="1200" style="border: 1px dotted grey"></canvas>');

                // отрисовка фигур
                var canvas = this.__canvas = new fabric.StaticCanvas('c');

                drawCenteredText(canvas, 310, 685, natalChart.firstName, 24, '#000000', -45, 'Arial');
                drawCenteredText(canvas, 305, 625, natalChart.birthDate, 24, '#000000', -45, 'Arial');

                //круги
                var circle1 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.acMcNotchOuterRadius,
                    fill: 'white',
                    stroke: 'black',
                    strokeWidth: 0.8
                });

                var circle2 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.starSignOuterRadius,
                    fill: '#ade3a6',
                    stroke: 'black',
                    strokeWidth: 1
                });

                var circle3 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.starSignNotchOuterRadius,
                    fill: 'white',
                    stroke: 'black',
                    strokeWidth: 1
                });

                var circle4 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.planetOuterRadius,
                    fill: '#cacee2',
                    stroke: 'black',
                    strokeWidth: 1
                });

                var circle5 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.aspectOuterRadius,
                    fill: 'white',
                    stroke: 'black',
                    strokeWidth: 1
                });


                var circle6 = new fabric.Circle({
                    left: 0,
                    top: 0,
                    radius: natalChart.centerOuterRadius,
                    fill: 'white',
                    stroke: 'black',
                    strokeWidth: 1
                });

                canvas.add(circle1, circle2, circle3, circle4, circle5, circle6);
                circle1.center();
                circle2.center();
                circle3.center();
                circle4.center();
                circle5.center();
                circle6.center();

                //насечки на кругах
                for (var i=0; i<360; i++) {
                    var newOffset = i + natalChart.ascendantDiff;
                    if (newOffset > 360) newOffset -= 360;
                    if (newOffset <= 0) newOffset += 360;
                    if ((i % 30) == 0) continue;

                    if ((i % 5) == 0) {
                        drawPartialPolarLine(canvas, i, natalChart.acMcNotchOuterRadius - natalChart.notchSize1, natalChart.notchSize2);
                        drawPartialPolarLine(canvas, i, natalChart.starSignNotchOuterRadius - natalChart.notchSize2, natalChart.notchSize2);
                    } else {
                        drawPartialPolarLine(canvas, i, natalChart.acMcNotchOuterRadius - natalChart.notchSize1, natalChart.notchSize1);
                        drawPartialPolarLine(canvas, i, natalChart.starSignNotchOuterRadius - natalChart.notchSize1, natalChart.notchSize1);
                    }
                }

                //Рисуем крупные насечки между зодиаками
                $.each(natalChart.signOffset, function(i,e){
                    drawPartialPolarLine(canvas, Math.round(e), natalChart.planetOuterRadius, natalChart.acMcNotchOuterRadius - natalChart.planetOuterRadius);
                });

                //Рисуем иконки знаки зодиаков
                var junction_font = new FontFace('HamburgSymbols', 'url(fonts/HamburgSymbols.ttf)');
                junction_font.load().then(function(loaded_face) {
                    $.each(natalChart.signOffset, function (i, e) {
                        var angle = e + natalChart.starSignSlice / 2;
                        var distance = natalChart.starSignOuterRadius - (natalChart.starSignOuterRadius - natalChart.starSignNotchOuterRadius) / 2;
                        var startX = 600 + Math.sin(getTanDeg(angle)) * distance - 24 / 2;
                        var startY = 600 - Math.cos(getTanDeg(angle)) * distance - 24 / 2;
                        canvas.add(new fabric.Text(natalChart.signFont[i], {
                            fontFamily: 'HamburgSymbols',
                            left: startX,
                            top: startY,
                            width: 25,
                            fill: natalChart.starSignColor,
                            fontSize: natalChart.starSignSize
                        }));
                    });
                });

                // рисуем планеты
                $.each(natalChart.planets, function(planet, details) {
                    if (natalChart.ignorePlanets.indexOf(planet) == -1) {
                        planetReletiveAngle = convertDegArrayToDec(details);
                        planetAbsoluteAngle = natalChart.signOffset[details.sign.toLowerCase()] + (natalChart.starSignSlice - planetReletiveAngle);
                        drawPartialPolarLine(canvas, planetAbsoluteAngle, natalChart.starSignNotchOuterRadius - natalChart.starSignNotchSize, natalChart.starSignNotchSize, '#0000FF');
                    }
                });

                // рисуем aspects
                $.each(natalChart.aspects, function(i, aspect) {
                    if (natalChart.ignoreAspects.indexOf(aspect.Planet1) == -1 && natalChart.ignoreAspects.indexOf(aspect.Planet2) == -1) {
                        // сплошные линии
                        if (natalChart.aspectStyles[aspect.Aspect].style == 'line') {
                            //Рисуем цветные линии
                            canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                                natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                                stroke: 'rgb('+natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                                strokeWidth: 1
                            }));
                        }
                        // пунктирные линии
                        else if (natalChart.aspectStyles[aspect.Aspect].style == 'dashed') {
                            canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                                natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                                stroke: 'rgb('+natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                                strokeWidth: 1,
                                strokeDashArray: [20,10]
                            }));
                        }
                        // точечные линии
                        else if (natalChart.aspectStyles[aspect.Aspect].style == 'dotted') {
                            canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                                natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                                stroke: 'rgb('+natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                                strokeWidth: 1,
                                strokeDashArray: [1,5]
                            }));
                        }
                    }
                });

                // рисуем позиции домов
                $.each(natalChart.houses, function(house, details) {
                    // Find the angle of the house
                    houseAbsoluteAngle = ((natalChart.houseCounter - 1) * 30) + 270;

                    // 30 Deg Houses
                    if (houseAbsoluteAngle >= 0) {
                        houseAbsoluteAngle = (((30 - (houseAbsoluteAngle % 30)) >= 15) ? (houseAbsoluteAngle - (houseAbsoluteAngle % 30)) : (houseAbsoluteAngle + (30 - (houseAbsoluteAngle % 30)))) + 0.001;
                    } else {
                        houseAbsoluteAngle = (((-30 - (houseAbsoluteAngle % (-30))) >= -15) ? (houseAbsoluteAngle + (-30) - (houseAbsoluteAngle % (-30))) : (houseAbsoluteAngle - (houseAbsoluteAngle % (-30)))) - 0.001;
                    }

                    if (house == 'Ascendant') {
                        ascendantAngel = houseAbsoluteAngle + 0.001;
                    } else {
                        houseAbsoluteAngle = ascendantAngel + natalChart.accumulatedAngel + 0.001;
                    }

                    //Рисуем линии от центра
                    if ((house == 'MC (Midheaven)') || (house == 'Ascendant')) {
                        drawPartialPolarLine(canvas, houseAbsoluteAngle, natalChart.starSignOuterRadius, natalChart.acMcNotchSize);
                        drawPartialPolarLine(canvas, houseAbsoluteAngle, natalChart.centerOuterRadius, natalChart.starSignNotchOuterRadius - natalChart.centerOuterRadius);
                        drawCenteredText(canvas, houseAbsoluteAngle, natalChart.acMcGlyphDistance, natalChart.signFont[house.toLowerCase()], natalChart.acMcGlyphFontSize, '#000000', 0, 'HamburgSymbols');
                        drawCenteredText(canvas, houseAbsoluteAngle - natalChart.acMcTextOffset, natalChart.acMcTextDistance, details.deg + "'" + details.min, natalChart.acMcTextFontSize, '#000000', 0, 'HamburgSymbols');

                        oppAngle = houseAbsoluteAngle - 180;
                        if (oppAngle <= 0) oppAngle += 360;
                        oppGlyph = (house == 'Ascendant' ? natalChart.signFont.descendant : natalChart.signFont.ic);

                        drawCenteredText(canvas, oppAngle, natalChart.acMcGlyphDistance, oppGlyph, natalChart.acMcGlyphFontSize, '#000000', 0, 'HamburgSymbols');
                        drawCenteredText(canvas, oppAngle - natalChart.acMcTextOffset, natalChart.acMcTextDistance, details.deg + "'" + details.min, natalChart.acMcTextFontSize);
                    } else {

                        drawPartialPolarLine(canvas, houseAbsoluteAngle, natalChart.centerOuterRadius, natalChart.planetOuterRadius - natalChart.centerOuterRadius);
                    }

                    natalChart.houseAngles[natalChart.houseCounter] = houseAbsoluteAngle;

                    natalChart.houseCounter++;

                    natalChart.accumulatedAngel -= 30;
                });

                // рисуем номера вокруг маленького круга
                $.each(natalChart.houseAngles, function(houseNumber, currentAngle) {
                    var nextHouseNumber = Number(houseNumber) + 1;
                    var nextAngle = (houseNumber < 12 ? natalChart.houseAngles[nextHouseNumber] : natalChart.houseAngles[1]);
                    var diff = (currentAngle > nextAngle ? currentAngle - nextAngle : (currentAngle + 360) - nextAngle);
                    var newAngle = currentAngle - (diff / 2);

                    if (newAngle <= 0) {
                        newAngle += 360;
                    }

                    drawCenteredText(canvas, newAngle, natalChart.houseTextDistance, houseNumber, natalChart.houseTextFontSize);
                });

                // рисуем иконки планет внутри синего круга
                $.each(natalChart.kSortedPlanets, function(i, planetDetails) {
                    drawCenteredText(canvas, planetDetails.angle, natalChart.planetGlyphDistance, natalChart.signFont[planetDetails.planet.toLowerCase()], natalChart.planetGlyphFontSize, '#000000', 0, 'HamburgSymbols');
                    drawCenteredText(canvas, planetDetails.angle, natalChart.planetDegTextDistance, natalChart.planets[planetDetails.planet].deg.toString(), natalChart.planetTextFontSize);
                    drawCenteredText(canvas, planetDetails.angle, natalChart.planetSignGlyphDistance, natalChart.signFont[natalChart.planets[planetDetails.planet].sign.toLowerCase()], natalChart.planetSignGlyphFontSize, '#000000', 0, 'HamburgSymbols');
                    drawCenteredText(canvas, planetDetails.angle, natalChart.planetMinTextDistance, natalChart.planets[planetDetails.planet].min.toString(), natalChart.planetTextFontSize);
                });
        	});
        }
    });
}

/**
 * рисование линии на акружности
 * @param canvas
 * @param angle
 * @param radius
 * @param length
 */
function drawPartialPolarLine(canvas, angle, radius, length, color = '#000000') {

    var startX = 600 + Math.sin(getTanDeg(angle)) * radius;
    var startY = 600 - Math.cos(getTanDeg(angle)) * radius;

    var endX = 600 + Math.sin(getTanDeg(angle)) * (radius + length);
    var endY = 600 - Math.cos(getTanDeg(angle)) * (radius + length);
    canvas.add(new fabric.Line([startX, startY, endX, endY], {
        stroke: color,
        strokeWidth: 1
    }));
}

function convertDegArrayToDec(properties) {
    return parseFloat(properties.deg) + ((parseFloat(properties.min) + (parseFloat(properties.sec) / 60)) / 60);
}

/**
 *
 * @param canvas
 * @param angle
 * @param radius
 * @param text
 * @param size
 * @param color
 * @param textAngle
 * @param fontFamily
 */
function drawCenteredText(canvas, angle, radius, text, size, color = '#000000', textAngle = 0, fontFamily = 'Arial') {
    var startX = 600 + Math.sin(getTanDeg(angle)) * radius;
    var startY = 600 - Math.cos(getTanDeg(angle)) * radius;

    var textWidth = getTextWidth(text, size);

    var junction_font = new FontFace('HamburgSymbols', 'url(fonts/HamburgSymbols.ttf)');
    junction_font.load().then(function(loaded_face) {
        canvas.add(new fabric.Text(text, {
            left: startX - textWidth / 2,
            top: startY - size / 2,
            width: textWidth,
            fill: color,
            fontSize: size,
            angle: textAngle,
            fontFamily: fontFamily,
        }));
        console.log(fontFamily + ' - ' + text);
    });
}

/**
 *
 * @param text
 * @param font
 * @returns {number}
 */
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}

/**
 *
 * @param text
 * @param font
 * @returns {number}
 */
function getTextHeight(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.height;
}

//преобразует градусы в радианы;
function getTanDeg(deg) {
    return rad = deg * Math.PI / 180;
}


function isDate(txtDate){
  var currVal = txtDate;
  if(currVal == '')
    return false;
   
  //Declare Regex
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
  
  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];
 
  if (dtMonth < 1 || dtMonth > 12)
      return false;
  else if (dtDay < 1 || dtDay> 31)
      return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }

  return true;

}