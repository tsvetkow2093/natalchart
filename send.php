<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);	
date_default_timezone_set('UTC');

require_once( "classes/Config.php" );
require_once( "classes/DB.php" );
require_once( "classes/AdGuru/Image/Toolbox.php" );
require_once( "classes/Acs/Location/Row.php" );
require_once( "classes/Acs/Location/Table.php" );
require_once( "classes/Acs/Timezone/Row.php" );
require_once( "classes/Acs/Timezone/Table.php" );
require_once( "classes/Acs/Timezone.php" );
require_once( "classes/Abstract.php" );
require_once( "classes/NatalChart.php" );
require_once( "classes/AdaptedNatalChart.php" );
require_once( "classes/RelocationChart.php" );
require_once( "classes/AdaptedToolbox.php" );
require_once( "classes/dompdf/dompdf_config.inc.php" );
require_once( "classes/dompdf/dompdf_config.inc.php" );

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once( "classes/PHPMailer/src/Exception.php" );
require_once( "classes/PHPMailer/src/PHPMailer.php" );
require_once( "classes/PHPMailer/src/SMTP.php" );

if(isset($_POST)){
	
	$startData = array(
		'current_first_name' => strval($_POST['current_first_name']),
		'current_last_name' => strval($_POST['current_last_name']),
		'acs_location_id' => strval($_POST['start_acs_location_id']),
		'birth_date' => array( 
			'year' => strval($_POST['birth_date_year']),
			'month' => AdaptedNatalChart::convertIntToStr($_POST['birth_date_month']),
			'day' => AdaptedNatalChart::convertIntToStr($_POST['birth_date_day'])
		),
		'birth_time' => array(
			'hour' => AdaptedNatalChart::convertIntToStr($_POST['birth_time_hour']),
			'minute' => strval($_POST['birth_time_minute']),
			'meridiem' => strval($_POST['birth_time_meridiem'])		
		),
	);
	$email = strval($_POST['email']);
	$nc = new AdaptedNatalChart( $startData );
	
	$report_url = "http://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=41&ReportVariation=rom&ReportFormat=JSON&Name1=".$_POST['current_first_name']."+".$_POST['current_last_name']."&BirthDate1=".$_POST["birth_date_month"]."/".$_POST["birth_date_day"]."/".$_POST["birth_date_year"]."&Name2=&BirthDate2=&StartDate=&Length=&AccountID=numerologist&AppID=CDS&MemberID=1234567890&V=2";
	$report = json_decode(file_get_contents($report_url), true);

	$dompdf = new Dompdf();
	$html = '<html><head><meta charset="UTF-8"><title>Personalized Natal Chart</title>';
	$html .= '<style type="text/css">';
	$html .= '.easy-autocomplete-container ul{ height: 150px; overflow: hidden; overflow-y: auto;}';
	$html .= '.report-paragraph{ padding: 30px 15px; margin-bottom: 30px;  }';
	$html .= '.page-title{ font-size: 16px; font-weight: 600; margin-top: 50px; }';
	$html .= '.page-subtitle{ font-size: 12px; font-weight: 400; margin: 5px auto; }';
	$html .= '</style></head><body><div class="container">';
	$html .= '<div class="row"><div class="col-xs-12 text-center"><img id="natal-chart-image" width="600" src="temp/'.$nc->drawChart( null, true ).'"/></div></div><div class="row"><hr><h1 class="text-center">Personality Profile</h1><hr><div id="natal-chart-report" class="col-xs-12 text-left">';
	foreach($report['report']['detail'][0]['section'] as $key => $val){
		$html .= '<div class="bg-info report-paragraph"><h3 class="text-center">'.$val['title'].'</h3>';
		foreach($val['paragraph'] as $key2 => $val2){
			$html .= '<h5><b>'.$val2['heading'].' <i>('.$val2['interaspect'].')</i></b></h5><p>'.$val2['text'].'</p>';
		}
		$html .= '</div>';

	}
	$html .= '</div></div></div></body></html>';
	$dompdf->load_html($html);
	$dompdf->render();
	$file = 'temp/'.time().'.pdf';
	file_put_contents($file, $dompdf->output(0));
	$mail = new PHPMailer();
	$mail->AddReplyTo("send@".$_SERVER['SERVER_NAME'],"Natal Chart");
	$mail->SetFrom("send@".$_SERVER['SERVER_NAME'], 'Natal Chart');
	$mail->AddReplyTo("send@".$_SERVER['SERVER_NAME'],"Natal Chart");
	$mail->AddAddress($email, $_POST['current_first_name']." ".$_POST['current_last_name']);
	$mail->Subject    = "My Natal Chart";
	$mail->MsgHTML('You natal card');
	$mail->AddAttachment($file); 
	var_dump($mail->Send());
}

?>